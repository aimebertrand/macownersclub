---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
tags:
- tag1
- tag2
categories:
- cat1
- cat2
keywords:
- kw1
- kw2
draft: true
---
