#+TITLE: Favorite Apps & Tools #8 - Docker
#+AUTHOR: Aimé Bertrand
#+DATE: [2022-01-04 Tue]
#+LANGUAGE: en
#+STARTUP: indent showall

#+HUGO_TAGS: apps tools software
#+HUGO_CATEGORIES: macos
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: posts
#+HUGO_WEIGHT: auto
#+HUGO_AUTO_SET_LASTMOD: t
#+EXPORT_FILE_NAME: favorite-apps-tools-8

* Tools
In contrast to the use cases mentioned in [[https://docs.docker.com/get-started/overview/][the official documentation]] my private use of Docker is much more mundane. I mostly need it to extend my tooling.

There is not much missing on my macOS machines to accommodate all of my needs. But in some rare cases I need to have access to some GNU/Linux exclusive software.

#+ATTR_HTML: :width 150px
[[/images/docker.png]]

On those occasional instances I have grown to rely on docker.

- Fire up a docker container with the [[https://hub.docker.com/search?type=image][appropriate image]].
- Install the needed software in case they are not built in.
- Run the task(s) and destroy the container as appropriate.

<span style="color:#d08770;">This is probably better illustrated with an example.</span>

* This is not a how to
I recently got into a pickle, where I needed to delete A LOT of duplicate mails in my [[https://macowners.club/posts/email-emacs-mu4e-macos/][maildir directory]].

After an extensive search on the internet, I decided that [[http://kremvax.net/howto/mail-duplicates.html][this wonderful post]] would help me the most.

The only hiccup was that the how-to uses =reformail= (included in =maildrop=) which was not available on macOS.

Well... Docker to the rescue.

- Fired up an Ubuntu image with mapped volumes to my maildir directory.
- Installed maildrop.
- Performed the steps as mentioned

And that was that.

* Conclusion
One cannot deny the usefulness of the container concept. Not just for Devs and/DevOps, but for any type of user.

At work Docker is as much a necessity as my machine itself. But I like Docker much more at home for these one-off tasks – similar to the example above.

But what about Windows software? Just... No.

* Footnotes
* COMMENT Local Variables                          :ARCHIVE:
# Local Variables:
# eval: (org-hugo-auto-export-mode)
# eval: (flyspell-mode)
# End:
