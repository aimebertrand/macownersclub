#+TITLE: Store SSH key (passphrase) into macOS Keychain
#+AUTHOR: Aimé Bertrand
#+DATE: [2023-07-26 Wed]
#+LANGUAGE: en
#+STARTUP: indent showall

#+HUGO_TAGS: macos ssh key keychain
#+HUGO_CATEGORIES: macos
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: posts
#+HUGO_AUTO_SET_LASTMOD: t

* Intro
The ssh agent on macOS can add your private key and the passphrase of this to the keychain ("Keychain Access.app").

This is nifty because you unlock the access with your normal login into the macOS user. Meaning you can use your key without typing in the passphrase for every session.

Keep in mind that you gain some comfort/convenience at the cost of some security.

* How-To
** Tell SSH to use the keychain
Add the following to the =~/.ssh/config= file:

#+begin_src conf
  Host *
    UseKeychain yes
    AddKeysToAgent yes
#+end_src

=UseKeychain yes= is the magic ingredient here.

** Add the key to the keychain

#+begin_src sh
  ssh-add -K ~/.ssh/[private-key]
#+end_src

You will be asked for your passphrase for the last time 😎.

The option =-K= (short for =--apple-use-keychain=) facilitates the addition to the agent using the keychain.

Et voilà!

* Footnotes
* COMMENT Local Variables                          :ARCHIVE:
# Local Variables:
# eval: (org-hugo-auto-export-mode)
# eval: (flyspell-mode)
# End:
