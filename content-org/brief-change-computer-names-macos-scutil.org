#+TITLE: Brief #2 - Change Mac name(s) with scutil
#+AUTHOR: Aimé Bertrand
#+DATE: [2022-09-15 Thu]
#+LANGUAGE: en
#+STARTUP: indent showall

#+HUGO_TAGS: macos hostname scutil
#+HUGO_CATEGORIES: macos
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: posts
#+HUGO_AUTO_SET_LASTMOD: t

* Intro
This one will show you (and future me) how to change the name of my Mac quickly on the Terminal.

We user the command line utility that comes built-in macOS =scutil=.

#+begin_example
  scutil – Manage system configuration parameters
#+end_example

* The How-To
The relevant portion of ~man scutil~ says:

#+begin_example
  --get pref
      Retrieves the specified preference.  The current value will be
      reported on standard output.

      Supported preferences include:

            ComputerName   The user-friendly name for the system.

            LocalHostName  The local (Bonjour) host name.

            HostName       The name associated with hostname(1) and
                           gethostname(3).

  --set pref [newval]
      Updates the specified preference with the new value.  If the new
      value is not specified on the command line then it will be read from
      standard input.

      Supported preferences include: ComputerName LocalHostName HostName

      The --set option requires super-user access.
#+end_example

With this we can change our Mac name(s) like so:

#+begin_src sh
  sudo scutil --set ComputerName MyAwesomeMac
  sudo scutil --set LocalHostName MyAwesomeMac
  sudo scutil --set HostName MyAwesomeMac
#+end_src

* Conclusion
This one is an extremely short one. But quite useful for *copy & paste*.

- Bonus :: For NetBIOS Networks (like WIN based networks; AD and the sorts) you can change the NetBIOS name like so:

#+begin_src sh
  sudo defaults write \
       /Library/Preferences/SystemConfiguration/com.apple.smb.server \
       NetBIOSName MyAwesomeMac
#+end_src

* Footnotes
* COMMENT Local Variables                          :ARCHIVE:
# Local Variables:
# eval: (org-hugo-auto-export-mode)
# eval: (flyspell-mode)
# End:
