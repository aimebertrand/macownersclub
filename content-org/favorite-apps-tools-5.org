#+TITLE: Favorite Apps & Tools #5 - Hammerspoon
#+AUTHOR: Aimé Bertrand
#+DATE: [2021-06-11 Fri]
#+LANGUAGE: en
#+STARTUP: indent showall

#+HUGO_TAGS: tools software
#+HUGO_CATEGORIES: apps
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: posts
#+HUGO_WEIGHT: auto
#+HUGO_AUTO_SET_LASTMOD: t
#+EXPORT_FILE_NAME: favorite-apps-tools-5

#+ATTR_HTML: :width 150px
[[/images/hammerspoon.png]]

* What?
[[http://www.hammerspoon.org/][Hammerspoon]] about Hammerspoon:

#+begin_quote
This is a tool for powerful automation of OS X. At its core, Hammerspoon is just a bridge between the operating system and a Lua scripting engine. What gives Hammerspoon its power is a set of extensions that expose specific pieces of system functionality, to the user.
#+end_quote

* Why?
Well that part was easy, but why do I need =Hammerspoon=? There is Applescript, Automator and even the whole power of a proper shell, the BSD General Commands and the sort built-in, right.

Pretty simple really: With =Hammerspoon=, I can combine all of the above and more within just the one tool. Plus if all fails, I can run arbitrary external scripts, as mention in my [[https://macowners.club/posts/favorite-apps-tools-1/][Favorite Apps & Tools introduction post]].

There is an added benefit of a reasonable – much more reasonable, than the built-in =System Preferences= – system for binding keyboard shortcuts for your automation.

Another Bonus is that you can have all, *all of you automation in one plain text file*. Now that is what I call portable.

* How?
Following are some examples, that I am implementing on my Mac.

** Launch Application
Of course I have an Application launcher, [[https://www.alfredapp.com/][Alfred]] – a separate post to follow. However some applications have a VIP status and need an exclusive manner to get started.

#+begin_src lua
  function launch_emacs ()
     hs.application.launchOrFocus("Emacs")
  end
  hs.hotkey.bind({"cmd", "alt", "ctrl"}, 'e', launch_emacs)
#+end_src

** Windows management
There are some dedicated applications/tools to bring tilling to macOS, but I deem them superfluous. I can implement almost all the functions within =Hammerspoon=.

For the sake of keeping this reasonably short, I will not list all of them here. If you are curious go find the entire setup on my [[https://gitlab.com/aimebertrand/dotfiles/-/tree/main/hammerspoon][Gitlab]].

#+begin_src lua
  ctalcm = {"cmd", "alt", "ctrl"}
  -- possible ass well: ctalcm = {"⌃", "⌥", "⌘"}

  -- send window to screen by number
  function moveWindowToDisplay(d)
     return function()
        local displays = hs.screen.allScreens()
        local win = hs.window.focusedWindow()
        win:moveToScreen(displays[d], false, true)
     end
  end

  hs.hotkey.bind(ctalcm, "1", moveWindowToDisplay(1))
  hs.hotkey.bind(ctalcm, "2", moveWindowToDisplay(2))
  hs.hotkey.bind(ctalcm, "3", moveWindowToDisplay(3))
#+end_src

** Emacs capture window from everywhere
With the following hotkey I can pull up an Emacs capture window from everywhere in macOS. Don't know Emacs? Check out [[https://macowners.club/posts/favorite-apps-tools-2/][my post about it]].

#+begin_src lua
  ctalcm = {"cmd", "alt", "ctrl"}

  -- capture window from everywhere
  hs.hotkey.bindSpec({ctalcm , "c"},
     function()
        hs.execute("/usr/local/bin/emacsclient -ne '(make-capture-frame)'")
     end
  )
#+end_src

* Conclusion
Despite the name, =Hammerspoon= is not the proverbial hammer. It is flexible enough to handle almost all the automation needs on my Mac.

Because of that I am in the process of moving all "legacy" configurations for automation into =Hammerspoon=.

*Not convinced yet. Head over to the [[http://www.hammerspoon.org/go][Getting started]] page and see some great examples.*

* Footnotes
* COMMENT Local Variables                          :ARCHIVE:
  # Local Variables:
  # eval: (org-hugo-auto-export-mode)
  # End:
