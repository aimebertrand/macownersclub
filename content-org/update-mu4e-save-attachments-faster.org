#+TITLE: Mu4e - save attachments faster - an update
#+AUTHOR: Aimé Bertrand
#+DATE: [2024-11-09 Sat]
#+LANGUAGE: en
#+STARTUP: indent showall

#+HUGO_TAGS: emacs mu4e attachement mail
#+HUGO_CATEGORIES: emacs
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: posts
#+HUGO_WEIGHT: auto
#+HUGO_AUTO_SET_LASTMOD: t
#+EXPORT_FILE_NAME: update-mu4e-save-attachments-faster


* My Issue
In an older post I wrote down my solution for [[https://macowners.club/posts/mu4e-save-attachments-faster-with-ivy/][saving multiple attachements at once]] without a completion from Mu4e. The reason being that my the bulk of use cases are to save all attachments at once.

In the rare cases I want to select an attachments out of many I can still use =mu4e-view-mime-part-action=.

Now I have encountered a few [[https://macowners.club/posts/mu4e-save-attachments-faster-with-ivy/#edits][breaking updates of Mu4e]] and for a while I have been dealing with another one. Granted, You are not supposed to use private function in an Emacs package/library in you own solution. So in this case I am the one to blame.

* My solution
The default command =mu4e-view-save-attachments= provides a way of selecting multiple files in the completion list, but even this kind of grinds my gear. I do not need the completion at all. See above.

This is the reason, why I went ahead and modified the command. Simply put, this removes the completion for a files. Then it *always* ask for the directory to save to starting with the =mu4e-attachment-dir=.

#+begin_src emacs-lisp
  (defun timu-mu4e-view-just-save-all-attachments ()
    "Save files from the current Mu4e view buffer.
  This applies to all MIME-parts that are \"attachment-like\" (have a filename),
  regardless of their disposition.

  This is a modified version of `mu4e-view-save-attachments'.
  It does not use `mu4e--completing-read' to select files, but just selects all.

  Also it always prompts for the directory to save to."
    (interactive)
    (let* ((parts (mu4e-view-mime-parts))
           (candidates  (seq-map
                           (lambda (fpart)
                             (cons ;; (filename . annotation)
                              (plist-get fpart :filename)
                              fpart))
                           (seq-filter
                            (lambda (part) (plist-get part :attachment-like))
                            parts)))
           (candidates (or candidates
                           (mu4e-warn "No attachments for this message")))
           (files (mapcar #'car candidates))
           (default-directory mu4e-attachment-dir)
           (custom-dir (read-directory-name
                                      "Save to directory: ")))
      (seq-do (lambda (fname)
                (let* ((part (cdr (assoc fname candidates)))
                       (path (mu4e--uniqify-file-name
                              (mu4e-join-paths
                               (or custom-dir (plist-get part :target-dir))
                               (plist-get part :filename)))))
                  (mm-save-part-to-file (plist-get part :handle) path)))
              files)))
#+end_src

* Conclusion
This might be a quick and dirty way of solving my issue, however it works really good for me. You are always free to hit me up with a better solution however. I am not the Emacs Lisp magician, so I am thinking a proper wiz might use an advice or hook or something.

* Footnotes
* COMMENT Local Variables                          :ARCHIVE:
# Local Variables:
# eval: (org-hugo-auto-export-mode)
# eval: (flyspell-mode)
# End:
