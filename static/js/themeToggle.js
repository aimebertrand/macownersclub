const themeToggle = document.getElementById('theme-toggle');
const cssLink = document.getElementById('theme-css');
const STORAGE_KEY = 'color-theme';
const storedTheme = localStorage.getItem(STORAGE_KEY);

themeToggle.addEventListener('click', (event) => {
  event.preventDefault();
  if (cssLink.getAttribute('href') === 'https://macowners.club/css/style-dark.css') {
    cssLink.setAttribute('href', 'https://macowners.club/css/style-light.css');
    localStorage.setItem(STORAGE_KEY, 'light');
  } else {
    cssLink.setAttribute('href', 'https://macowners.club/css/style-dark.css');
    localStorage.setItem(STORAGE_KEY, 'dark');
  }
});

if (storedTheme === 'light') {
  cssLink.setAttribute('href', 'https://macowners.club/css/style-light.css');
} else {
  cssLink.setAttribute('href', 'https://macowners.club/css/style-dark.css');
}
