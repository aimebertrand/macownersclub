+++
title = "Favorite Apps & Tools #3 - Vim"
author = ["Aimé Bertrand"]
date = 2021-05-27T00:00:00+02:00
lastmod = 2021-05-28T01:02:07+02:00
tags = ["tools", "software"]
categories = ["apps"]
draft = false
+++

## Warning {#warning}

This will be long and probably for some stretches fairly incoherent. But if you are anything like me it will be fun – probably. If not, do not despair, read on and at the end you will find links to resources by people more versed.


## Start {#start}

{{< figure src="/images/vim_start.png" width="800px" >}}

How does one start using [Vim](https://www.vim.org/). Isn't it that cli program, that one gets stuck in trying to edit a cronjob? The editor that those annoying elitists in comments and forums keep talking about.

Yes that is how I used to think. I simply couldn't see the appeal. I have my GUI editor on my machine and it worked perfectly. If I need to edit a longer or more intricate file. Just scp it, edit it locally and scp it back. Simple enough. Plus the little documentation I managed to even look at suggested that the barriers to entry was pretty high.

Granted there is a truth to the last arguments. The learning curve to using Vim is substantial. **But like most things worth a while, well it is worth it to invest the time.**

{{< figure src="/images/emacs_learning_curve.png" width="800px" >}}


## Why {#why}

Honestly I started trying to use Vim because of the drive to try out new stuff and admittedly there was a hint of spite as well. There is no better motivation than people saying that something was hard to do.

Around 2 years later I am writing a post about vim being one of my favorite tools on my Mac.


## Vim {#vim}

In my [last post](https://macowners.club/posts/favorite-apps-tools-2/) I struggled to describe the program I was writing about (Emacs). This time I am starting with a quote from the [official Homepage](https://www.vim.org/).

> Vim is a highly configurable text editor built to make creating and changing any kind of text very efficient. It is included as "vi" with most UNIX systems and with Apple OS X.

Since Vim is preinstalled on most Operating Systems, it is easy to access. Most people using it including me just launch it in their terminal emulator or on the tty. Everyone will tell you how advantageous the keyboard focused usage is. And I agree.

Vim is extensible with a [plethora of plugins](https://vimawesome.com/) being available. These include themes, additions to built-ins commands, handling of VCS like git, navigation additions like tree file explorers, searching through fuzzy matching and more.

{{< figure src="/images/vim_plugins.png" width="800px" >}}


## Chops {#chops}

However the true strength of Vim is the modal text editing paradigm. I will not attempt to fully describe it in this post. I will just try to demonstrate the power of modes with few examples.

**So the following lines are my two cents as an enthusiastic layman.**


### Movements {#movements}

It sounds somewhat counter intuitive, but moving around text with the keyboard rather that with the mouse is faster. After all with the mouse pointer one can go precisely to the desired location in the text, right.

It does take some getting used to but with all the movement commands of Vim, I am now able to better navigate all text that I am in.

Here are some examples:

`H` (mnemonic: high)
: Move to the top of the screen (the visual part) in the Editor.

`L` (low)
: Move to the bottom of the screen in the Editor.

`M` (middle)
: Move to the middle of the screen in the Editor.

`w` (word)
: Jump forward to the start of the next word.

`b` (word)
: Jump backward to the start of a word.

`0` (0 position)
: Go to the beginning of the line.

`$`
: Go to the end of the line.

`b` (back)
: Jump backward to the start of a word.

`gg`
: Go to the top of the document.

`G`
: Go to the end of the document.

{{< figure src="/images/vim_movements.gif" width="800px" >}}

**There are of course quite a lot more, but they are definitely out of scope for this post and the way it is setup.**


### Normal Mode {#normal-mode}

The first and default mode, _**normal mode**_ is primarily meant for navigation around text and for atomic edits of existing text. This mode has a whole bunch of commands that help edit, change, substitute or modify text with mindbending accuracy.

You Edit most of the things in normal mode like you form sentences. Caveat: We are talking about English sentences here.

For some examples:

Type `cw` (change word) on a word!
: This will delete text from the cursor position to the end of the word and bring you in the position to type in something.

Type `ciw` (change inside word) on a word!
: This will delete the entire word till the and bring you in the position to type in something.

Type `diw` (delete inside word) on a word!
: This will delete the whole word.

Bonus
: you can type a numeric number (n) before any of the above and the action will by applied n-times. Say `2diw` would delete the current word and the next.

`c` in those examples is the verb. `w` is of course the object in the sentence and a `text object` in Vim-Speak. This can be applied to more text objects, `s` (sentence), `p` (paragraph), `)` (parentheses) to mention a few examples.

{{< figure src="/images/vim_normal_mode.gif" width="800px" >}}

Second Bonus
: You can extend the built text objects with plugins of course thus extending your grammar.

Other frequently used "verbs":

`y` (yank)
: Yank is essentially copy.

`p` (paste)
: No clarification needed.

`r` (replace)
: Replace character under cursor.


### Insert Mode {#insert-mode}

The second important mode, _**insert mode**_ is the mode on changes into, to type in text like in most conventional editors. There are of course other wonderful commands in this mode, since we are talking about a brilliant tool.

Many of the key mappings for the commands in insert mode include a modifier key, `Ctrl` mostly. These – like all key mappings in Vim – can be modified to your liking. Say you want to use `Alt` as a modifier. Go for it nothing stopping you.

{{< figure src="/images/vim_insert_mode.png" width="800px" >}}

Some examples here as well:

`Ctrl+w`
: This would delete the word before the cursor.

`Ctrl+t`
: This would indent the current line.


### Command-line Mode {#command-line-mode}

The third mode, the _**command-line mode**_ helps mostly with lengthy commands, that can be pre-composed as well.

Few examples:

`:g/^$/d`
: This would delete all the blank lines in the document.

`:%s/foo/bar/`
: Substitute all the first occurrences of `foo` with `bar` for each line. With a `g` at the end this will apply to all occurrences.

`:read !ls ~/Desktop`
: Insert the output of `ls ~/Desktop` into the document.

`:h`
: Display the `VIM - main help file`.

Bonus
: Most commands work with regular expressions as well. This extends the power enormously.

{{< figure src="/images/vim_cmd_mode.gif" width="800px" >}}

**There are more modes to Vim, but since this is not an all encompassing post, I will omit them. This is just some praises and ramblings about the program.**


## Conclusion {#conclusion}

In my mind there is no better way of editing text than the Vim way. It is fast, precise and incredibly flexible.

With the capability for changing most behaviors and keybindings of the editor you can bend it to your will.

Also due to the vast number of available plugins and extensions most of imaginable functionalities can be added.

> Most important though. Do not get fooled by most info on the internet. Vim is not only useful for editing code. It is wonderfully suited for any kind of text/prose as well.


## Resources {#resources}

[Vim User Manual](http://vimdoc.sourceforge.net/htmldoc/usr%5Ftoc.html)
: The official user manual.

[Vim Cheat Sheet](https://vim.rtorr.com/)
: enough said, right?

[Vim FAQ](https://vimhelp.org/vim%5Ffaq.txt.html)
: FAQ Collection on the usage.

[Vim Tips Wiki](https://vim.fandom.com/wiki/Vim%5FTips%5FWiki)
: Tips and tricks.
