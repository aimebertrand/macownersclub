+++
title = "Setq vs customize-set-variable"
author = ["Aimé Bertrand"]
date = 2021-10-26T00:00:00+02:00
lastmod = 2021-10-26T17:18:48+02:00
tags = ["emacs", "variables", "config"]
categories = ["emacs"]
draft = false
+++

## Why this {#why-this}

Listen... There is no lack of confusion about the proper way of setting variables in ones Emacs config. There are far more ways to set these than the two mentioned in the title, which does not make matters easier.

-   `set`
-   `setq`
-   `set-default`
-   `customize-set-value`
-   `customize-set-variable`
-   And probably more that I do not know about

The question becomes:

> What is the best practice here?

Most people – including me – use `setq` to set values and are done with it. This mostly does the trick and in my experience has not have adverse effects in my config so far.

However it is quite disputed weather this is the proper way. And in my quest to do things the right way, I need to remedy this issue. At least as far as my current skill set will carry me.

Few mentions online do discuss this way better that I could.

-   [Advantages of setting variables with setq instead of custom.el?](https://emacs.stackexchange.com/questions/102/advantages-of-setting-variables-with-setq-instead-of-custom-el)
-   [Using custom-set-variables programmatically](https://www.reddit.com/r/emacs/comments/67pzh5/using%5Fcustomsetvariables%5Fprogrammatically/)
-   [Confusing about the Emacs custom system](https://stackoverflow.com/questions/12058717/confusing-about-the-emacs-custom-system)

One things that I can say with reasonable certainty though: `custom-set-variables` is definitely not the answer. The automatic system in Emacs even mentions it in the generated `custom-file`.

```emacs-lisp
;; custom-set-variables was added by Custom.
;; If you edit it by hand, you could mess it up, so be careful.
;; Your init file should contain only one such instance.
;; If there is more than one, they won't work right.
```


## Reasoning {#reasoning}

There seems to be a consensus amongst Emacs wizes that the correct way of going about changing/setting variable values would be using the `Custom-mode` UI with `M-x customize RET`.

This is a hard pass for me, because of the tedious nature of the process. Plus I don't want to accept, that There are no other means to get to the solution in an elegant way.


## Solution {#solution}

As it turns out, There are variables with a `Custom-mode` interface and other without. Those with an interface are declared in the packages with `defcustom`. Those without with `defvar`. So far so good.

This makes matters a little easier because the "defvar-variables" can be set with `setq` and that is it.

The hard part comes with the "defcustom-variables". This part I do not understand completely, but the gist is that some variables do trigger additional code to be executed when set through the Custom system.

Reading through various posts, documentations and more did not help my confusion. This forced me to choose to go the trial-and-error way.

-   <span style="color:#d08770;">step one: use one of the functions mentioned further up.</span>
-   <span style="color:#d08770;">step two: check the result in the `Custom-mode` UI.</span>

Judging by the result in Customize interface `set`, `setq`, `setq-default` or `customize-set-value` are obviously wrong for the task:

<span style="color:#a3be8c;">CHANGED outside Customize.</span>

{{< figure src="/images/cus-edit-1.png" width="600px" >}}

When using `customize-set-variable` in the form ...

```emacs-lisp
(customize-set-variable 'org-directory "~/org")
```

... I get the following result

<span style="color:#a3be8c;">SET for current session only.</span>

{{< figure src="/images/cus-edit-2.png" width="600px" >}}

Well this seems to be the right way for me for now. The config is properly recognised and set in the Customize interface. The values are persistent for the session and since my config is always loaded on startup these are the same across sessions.

Even inspecting my `custom-file` the values are properly reflected.


## Conclusion {#conclusion}

Now I do not have 100% confidence, that my way of going about this is right, but I know that this is certainly better that just a blanket use of `setq`.

Discuss! ;)