+++
title = "Using Emacs support for Xwidgets on macOS"
author = ["Aimé Bertrand"]
date = 2022-05-19T00:00:00+02:00
lastmod = 2022-05-19T19:07:06+02:00
tags = ["emacs", "macos", "xwidgets", "webkit"]
categories = ["emacs"]
draft = false
+++

## Reason {#reason}

Let's be honest as Emacs users. Sometimes a fully fledged browser experience or display of content is called for and – dare I say – superior. Be it for either displaying dynamic content, (moving) images, well designed websites and the sort.

For that reason we sometimes need to go to a browser. This however means a possibly unwanted context switch and worse of all leaving Emacs.

In comes the Xwidgets framework. This allows appropriate packages to implement `WebKit Widgets`. Meaning that WebKit based content can be embedded in Emacs. And though the magic of it all we are allowed to stay a bit longer in our favorite tool.


## How I use it {#how-i-use-it}

Let me try and give you some example to illustrate in which cases WebKit Widgets are useful to me.


### Browser {#browser}

The following image shows a YouTube video running inside Emacs. Sound, caption and all. The only thing that does not work here as far as I can see is full screen. But that is just nagging at that point.

{{< figure src="/images/xwidgets-browser-2.png" width="800px" >}}


### HTML email display {#html-email-display}

By default mu4e does a fantastic job of displaying HTML emails. And honestly [I prefer the plain text display of emails](https://macowners.club/posts/text-based-email-workflow-macos/) anyways. However there are some cases, where one wants to see the email as it was intended to be displayed.

{{< figure src="/images/xwidgets-mu4e.png" width="800px" >}}

This is not a default behavior as far as I can see. Plus, the code for the functionality of Xwidget view action in mu4e is being – or rather has been deprecated. However there is a PR with a library that can be used to regain that "power".

I added the `mu4e-view-xwidget.el` file into my `load-path` and put the following snippet into my configuration. with that I can toggle the display of my email in an Xwidget.

```emacs-lisp
(add-to-list 'mu4e-headers-actions
             '("xWidget" . mu4e-action-view-with-xwidget) t)
(add-to-list 'mu4e-view-actions
             '("xWidget" . mu4e-action-view-with-xwidget) t)
```


### Editing content with preview {#editing-content-with-preview}

The following image is actually showing the live preview while I am typing it – inside of Emacs. I use [Hugo](https://gohugo.io) for the static site, in case you are asking.

{{< figure src="/images/xwidgets-hugo-preview.png" width="800px" >}}


### Preview markdown and org-mode with a package {#preview-markdown-and-org-mode-with-a-package}

Another useful preview tool which works using Xwidgets is [grip-mode](https://melpa.org/#/grip-mode), which I use extensively.

{{< figure src="/images/xwidgets-grip-mode.png" width="800px" >}}


## Conclusion {#conclusion}

Now I could have stopped with the first example. The ability of displaying most websites or html content entails quite a bit of power. One can only imagine the possibilities.

I am not saying that you should move all of your browsing into Emacs. I do not do this myself, but some fast searches or quick lookup work great in Emacs. And frankly this is but a taste for the potential.


## Meta {#meta}

-   **Machine:** MacBook Pro 13" M1
-   **OS:** macOS Monterey
-   **Emacs Version:** GNU Emacs 28.1
-   **Emacs installation using [homebrew](https://brew.sh):**

<!--listend-->

```sh
brew tap railwaycat/emacsmacport
brew install emacs-mac --with-native-comp --with-xwidgets --with-natural-title-bar
```