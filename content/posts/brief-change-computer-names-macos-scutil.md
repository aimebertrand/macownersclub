+++
title = "Brief #2 - Change Mac name(s) with scutil"
author = ["Aimé Bertrand"]
date = 2022-09-15T00:00:00+02:00
lastmod = 2022-09-15T14:30:09+02:00
tags = ["macos", "hostname", "scutil"]
categories = ["macos"]
draft = false
+++

## Intro {#intro}

This one will show you (and future me) how to change the name of my Mac quickly on the Terminal.

We user the command line utility that comes built-in macOS `scutil`.

```text
scutil – Manage system configuration parameters
```


## The How-To {#the-how-to}

The relevant portion of `man scutil` says:

```text
--get pref
    Retrieves the specified preference.  The current value will be
    reported on standard output.

    Supported preferences include:

          ComputerName   The user-friendly name for the system.

          LocalHostName  The local (Bonjour) host name.

          HostName       The name associated with hostname(1) and
                         gethostname(3).

--set pref [newval]
    Updates the specified preference with the new value.  If the new
    value is not specified on the command line then it will be read from
    standard input.

    Supported preferences include: ComputerName LocalHostName HostName

    The --set option requires super-user access.
```

With this we can change our Mac name(s) like so:

```sh
sudo scutil --set ComputerName MyAwesomeMac
sudo scutil --set LocalHostName MyAwesomeMac
sudo scutil --set HostName MyAwesomeMac
```


## Conclusion {#conclusion}

This one is an extremely short one. But quite useful for **copy &amp; paste**.

Bonus
: For NetBIOS Networks (like WIN based networks; AD and the sorts) you can change the NetBIOS name like so:

<!--listend-->

```sh
sudo defaults write \
     /Library/Preferences/SystemConfiguration/com.apple.smb.server \
     NetBIOSName MyAwesomeMac
```