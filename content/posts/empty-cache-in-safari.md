+++
title = "Empty the Cache in Safari"
author = ["Aimé Bertrand"]
date = 2020-12-20T00:00:00+01:00
lastmod = 2021-04-23T14:59:13+02:00
tags = ["safari", "web"]
categories = ["macos"]
draft = false
+++

## Objective {#objective}

Today for a short and simple one.

I like _Safari_ as a browser. I like using default macOS applications as much as possible. _Safari_ is built-in into macOS and I have been using it for decades without missing any functionality. Granted, I am not web developer, I don't need much plugins or extensions.

One thing annoys me though. By defaults the menu for emptying the cache is not active. At some point in the OSX/macOS Upgrades it got hidden from the default menu. Emptying the browser cache is useful of course, when one is blogging – I use [Hugo](https://gohugo.io) – to preview changes on the sites.
I do not want to have false negatives in my changes, because the browser still has the old state in the cache.


## Solution {#solution}

Well to activate the menu is quite simple.

Got to the menu _Preferences..._ (1 & 2 in the image bellow) or just `⌘ + ,`. Select the _Advanced_ (3) tab and the check the box for _Show Develop menu in menu bar_ (4).

{{< figure src="/images/safari_dev_menu.png" width="600" >}}

Now that the "Develop" menu is visible, emptying the cache is as simple as the following image shows. One can of course use the Keyboard shortcut `⌥ + ⌘ + E`.

{{< figure src="/images/sadari_empty_cache.png" width="600" >}}
