+++
title = "Store SSH key (passphrase) into macOS Keychain"
author = ["Aimé Bertrand"]
date = 2023-07-26T00:00:00+02:00
lastmod = 2023-07-26T21:10:27+02:00
tags = ["macos", "ssh", "key", "keychain"]
categories = ["macos"]
draft = false
+++

## Intro {#intro}

The ssh agent on macOS can add your private key and the passphrase of this to the keychain ("Keychain Access.app").

This is nifty because you unlock the access with your normal login into the macOS user. Meaning you can use your key without typing in the passphrase for every session.

Keep in mind that you gain some comfort/convenience at the cost of some security.


## How-To {#how-to}


### Tell SSH to use the keychain {#tell-ssh-to-use-the-keychain}

Add the following to the `~/.ssh/config` file:

```cfg
Host *
  UseKeychain yes
  AddKeysToAgent yes
```

`UseKeychain yes` is the magic ingredient here.


### Add the key to the keychain {#add-the-key-to-the-keychain}

```sh
ssh-add -K ~/.ssh/[private-key]
```

You will be asked for your passphrase for the last time 😎.

The option `-K` (short for `--apple-use-keychain`) facilitates the addition to the agent using the keychain.

Et voilà!
