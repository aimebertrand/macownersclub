+++
title = "Learning Python #2"
author = ["Aimé Bertrand"]
date = 2021-02-27T00:00:00+01:00
lastmod = 2021-04-23T14:59:54+02:00
tags = ["coding", "learning", "development"]
categories = ["python"]
draft = false
+++

In my last [post (Learning Python #1)](https://macowners.club/posts/learning-python-1/) I was talking about implementing functions and classes into my script for practise purposes. Well this is it...


## Functions {#functions}

The only place I found a reasonable use of a function was to get values for the attributes in my only custom class in this script.

The script has a source directory hard coded and the files in the directory get sorted. They get filtered and get passed to the script in a list [(lines 22 to 27)](https://macowners.club/posts/learning-python-1/#starting-to-code-in-python).

I then need to get the creation year and month, the source and destination path of each list item with it being the filename.

The following function gets this info for me and puts them into a list.

```python
def getInfo(file):
    fyear = file.split('-')[0]
    fmonth = file.split('-')[1]
    fsrc = os.path.join(work_dir, file)
    fdest = os.path.join(os.path.join(dest_dir, fyear, fmonth), file)
    return [fyear, fmonth, fsrc, fdest]
```


## Class and method {#class-and-method}

Since I wanted to practise working with classes as well, I managed to cram in a class for the files to be sorted.

Essentially I just wanted to implement a class for the files, where the objects have the attributes of file creation year, file creation month, file source path and files destination path.

I wanted also to use at least a class method for moving the filtered files from the source path to the destination path.

This is the code for the class part:

```python
class FilesToMove:
    def __init__(self, file_year, file_month, file_src, file_dest):
        self.file_year = file_year
        self.file_month = file_month
        self.file_src = file_src
        self.file_dest = file_dest

    def moveFile(self):
        os.rename(self.file_src, self.file_dest)
```


## The complete script {#the-complete-script}

Putting all together I got a script.

I am quite happy with it, but I ll continue hacking on it.

```python
#!/usr/local/bin/python3

import os

# 1. Variables for needed paths.
home_dir = os.environ['HOME']
work_dir = os.path.join(home_dir, 'projects/python/file-sorting/inbox')
dest_dir = os.path.join(home_dir, 'projects/python/file-sorting/sorted')


# 2. Check if files have date in name. Put those with date in a list.
year_list = list(range(1900, 2100))

string_year_list = list()
for y in year_list:
    string_year_list.append(str(y))

months = ['01', '02', '03', '04', '05',
      '06', '07', '08', '09', '10', '11', '12']

work_dir_content = os.listdir(work_dir)
files_to_move = list()
for fl in work_dir_content:
    sfl = fl.split('-')
    if os.path.isfile(os.path.join(work_dir, fl)):
    if sfl[0] in string_year_list and sfl[1] in months:
        files_to_move.append(fl)


# 3. Functions for creating class attributes.
def getInfo(file):
    fyear = file.split('-')[0]
    fmonth = file.split('-')[1]
    fsrc = os.path.join(work_dir, file)
    fdest = os.path.join(os.path.join(dest_dir, fyear, fmonth), file)
    return [fyear, fmonth, fsrc, fdest]


# 4. Class for the files.
class FilesToMove:
    def __init__(self, file_year, file_month, file_src, file_dest):
    self.file_year = file_year
    self.file_month = file_month
    self.file_src = file_src
    self.file_dest = file_dest

    def moveFile(self):
    os.rename(self.file_src, self.file_dest)


# 3. Check for existance for Destination directory.
# If it exists, move the file. If it does not, create it and move file.
for f in files_to_move:
    f = FilesToMove(getInfo(f)[0], getInfo(f)[1], getInfo(f)[2], getInfo(f)[3])
    if os.path.isdir(os.path.dirname(f.file_dest)):
    f.moveFile()
    else:
    os.makedirs(os.path.dirname(f.file_dest))
    f.moveFile()
```


## Next steps for this script {#next-steps-for-this-script}

-   Add conditions to avoid duplicates
-   What ever else I will come up with
