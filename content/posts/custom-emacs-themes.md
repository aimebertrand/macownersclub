+++
title = "Custom Emacs themes"
author = ["Aimé Bertrand"]
date = 2023-02-11T00:00:00+01:00
lastmod = 2023-02-14T22:51:43+01:00
tags = ["emacs", "themes", "lisp"]
categories = ["emacs"]
draft = false
+++

## Yet another Emacs theme? {#yet-another-emacs-theme}

We are talking about GNU Emacs here.

GNU software are "Supported by the [Free Software Foundation](https://www.gnu.org/#mission-statement)" and the FSF is a proponent of free software. Since one of the corner stones of free software is contribution I didn't want to just consume Emacs.

Now I am no wizard in writing Emacs Lisp. Far from it. However I wanted to create a few packages that might be of use to me and to some other people – how ever little the number is.

Creating a theme seems a good starting point, right?


## The themes {#the-themes}


### timu-caribbean-theme {#timu-caribbean-theme}

Color theme with cyan as a dominant color.

See the [readme file](https://gitlab.com/aimebertrand/timu-caribbean-theme/-/blob/main/README.org) for customization options.

{{< figure src="https://melpa.org/packages/timu-caribbean-theme-badge.svg" link="https://melpa.org/#/timu-caribbean-theme" >}}

<p align="center"><img src="https://gitlab.com/aimebertrand/timu-caribbean-theme/-/raw/main/img/timu-caribbean.png" width="100%"/></p>


### timu-macos-theme {#timu-macos-theme}

Color theme inspired by the macOS UI.

This theme has a **dark and a light "flavour"**. Customize `timu-macos-flavour` to control it.

For further information on the theme you can take a look at the [readme file](https://gitlab.com/aimebertrand/timu-macos-theme/-/blob/main/README.org).

{{< figure src="https://melpa.org/packages/timu-macos-theme-badge.svg" link="https://melpa.org/#/timu-macos-theme" >}}

<p align="center"><img src="https://gitlab.com/aimebertrand/timu-macos-theme/-/raw/main/img/timu-macos-theme-darker-1.png" width="100%"/></p>

<p align="center"><img src="https://gitlab.com/aimebertrand/timu-macos-theme/-/raw/main/img/timu-macos-theme-lighter-1.png" width="100%"/></p>


### timu-rouge-theme {#timu-rouge-theme}

Color theme inspired by the [Rouge Theme for VSCode](https://marketplace.visualstudio.com/items?itemName=josef.rouge-theme).

You can read up on the options of the theme in the [readme file](https://gitlab.com/aimebertrand/timu-rouge-theme/-/blob/main/README.org).

{{< figure src="https://melpa.org/packages/timu-rouge-theme-badge.svg" link="https://melpa.org/#/timu-rouge-theme" >}}

<p align="center"><img src="https://gitlab.com/aimebertrand/timu-rouge-theme/-/raw/main/img/timu-rouge.png" width="100%"/></p>


### timu-spacegrey-theme {#timu-spacegrey-theme}

Emacs color theme inspired by the [Spacegray theme in Sublime Text](https://packagecontrol.io/packages/Theme%20-%20Spacegray).

This theme also has a **dark and a light "flavour"**. You just have to customize the variable `timu-spacegrey-flavour`.

There are a few more options to this theme. You can find these in the [readme file](https://gitlab.com/aimebertrand/timu-spacegrey-theme/-/blob/main/README.org).

{{< figure src="https://melpa.org/packages/timu-spacegrey-theme-badge.svg" link="https://melpa.org/#/timu-spacegrey-theme" >}}

<p align="center"><img src="https://gitlab.com/aimebertrand/timu-spacegrey-theme/-/raw/main/img/timu-spacegrey-1.png" width="100%"/></p>

<p align="center"><img src="https://gitlab.com/aimebertrand/timu-spacegrey-theme/-/raw/main/img/timu-spacegrey-2.png" width="100%"/></p>


## Credit &amp; inspiration {#credit-and-inspiration}

**Note:** There are Emacs ports of some of these themes already, however I wanted support for more packages and a few different colors/faces. I also will be slowly adding more and more customization options.

-   <https://github.com/tonyaldon/emacs.d/blob/master/themes>
-   <https://github.com/doomemacs/themes/blob/master/themes>


## Conclusion {#conclusion}

I know there is already an immense amount of themes available in the various package archives.

Still, creating these themes has been quite a lesson for me to get to know some of the infrastructure around Emacs. Plus I can now write a little more Emacs Lisp. Worth it!
