+++
title = "Favorite Apps & Tools #1 - Intro"
author = ["Aimé Bertrand"]
date = 2021-05-12T00:00:00+02:00
lastmod = 2025-01-21T22:40:37+01:00
tags = ["tools", "software"]
categories = ["apps"]
draft = false
+++

## Why {#why}

I am on macOS all day everyday. I like the OS, my job revolves around Apple products and the quality control of the hardware and software is off the charts.

Plus with macOS it is easier for me to run the other 2 of the big three OS on the same machine. You don't believe me? Try and run a macOS VM on Windows or Linux.

Still I am a big fan of open-source software. So much so, that most of my favorite tools on my MacBook are open-source. And I want to share some of them with the intention of maybe helping someone out there.

Granted, not all the tools that will be presented here are free and open-source, but they will make out a huge chunck of the series. You will see!

I have been wanting to make this series to throw my hat in the ring of opinions, but I barely had the time for it. Now listing the first bunch I hope to put myself under pressure to finally go for it.


## What {#what}

The first list of apps and tools I want to talk about in the following weeks:


### [Emacs](https://macowners.club/posts/favorite-apps-tools-2/) {#emacs}

<img style="display:inline;float:left;margin:10px 0px;" src="/images/emacs.png" width="75"/>

By far my favorite application, ney universal tool, is Emacs. I spend quite a lot of time of my day at work and at home in Emacs. Email, RSS, writing, reading, scripting, DTG and much more.

Even this post is being written for hugo inside Emacs's org-mode. Which by itself might merit a post in the future. You know what, I take back the "might", this is a definite "does".


### [Vim](https://macowners.club/posts/favorite-apps-tools-3/) {#vim}

<img style="display:inline;float:left;margin:10px 0px;" src="/images/vim.png" width="75"/>

Nothing beats Vim's modal text editing power (I use Evil in Emacs as well, ssshhh!). One can fly around thousend of lines of code/text in a giffy. I am not even at 40% of what Vim can do, but I imagine myself to be way way faster than I was around 1,5 years ago before I got "baptized".


### [Fzf](https://macowners.club/posts/favorite-apps-tools-4/) {#fzf}

<img style="display:inline;float:left;margin:10px 0px;" src="/images/fzf.png" width="75"/>

This is a great tool for the cli. It does one thing and one thing only. But it does it brilliantly. It takes a list as input and lets you fuzzy search/match the lines in the list and spits out the selection(s) to the stdout.

Just imagine the possibilities. More about it in the post to come.


### [Hammerspoon](https://macowners.club/posts/favorite-apps-tools-5/) {#hammerspoon}

<img style="display:inline;float:left;margin:10px 0px;" src="/images/hammerspoon.png" width="75"/>

Hammerspoon is another fantastic application, that extends your mac functionality mostly through the incredible automation chops.

-   Keybindings
-   Automation
-   Window Management
-   Run arbitrary scripts too
-   etc.


### [Alfred](https://macowners.club/posts/favorite-apps-tools-6/) {#alfred}

<img style="display:inline;float:left;margin:10px 0px;" src="/images/alfred.png" width="75"/>

On the surface one might think that Alfred.app is just a replacement for the macOS built in Spotlight. But it is much more. It is an App launcher, search tool, bookmark, snippets &amp; clipboard manager etc.

When you add the workflow capabilities to it though, sky is the limit.


### [Affinity Suite](https://macowners.club/posts/favorite-apps-tools-7/) {#affinity-suite}

<img style="display:inline;float:left;margin:10px 0px;" src="/images/affinity.png" width="75"/>

This is a worthy and professional grade set of Application (Affinity Photo, Affinity Designer, Affinity Publisher) to replace the Adobe Creative Cloud Design suite. Enough said.


### [Docker](https://macowners.club/posts/favorite-apps-tools-8/) {#docker}

<img style="display:inline;float:left;margin:10px 0px;" src="/images/docker.png" width="75"/>

Well, Docker let's you run almost all (Dev) Tools under the sky, mostly Linux based, in container environments.

I even run Linux X11-based GUI Applications with the help of the XQuartz project on my mac. Whaaat?


## Conclusion {#conclusion}

Of course this is just a subset of the stuff I want to talk about in this series. But I will have to get started first, right.
