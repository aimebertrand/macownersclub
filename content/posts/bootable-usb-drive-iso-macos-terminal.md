+++
title = "Bootable USB drive from ISO on macOS Terminal"
author = ["Aimé Bertrand"]
date = 2022-09-21T00:00:00+02:00
lastmod = 2022-09-22T10:12:22+02:00
tags = ["macos", "usb", "drive", "iso"]
categories = ["macos"]
draft = false
+++

## The "Why" {#the-why}

One still need to create a bootable USB drive from an ISO from time to time. Mostly to install an OS from the drive.

Now there are quite a few GUI Apps (e.g. [Etcher](https://www.balena.io/etcher/)) to facilitate that. However I see no need to install an application, that I might only need every other moon.

The solution is to use macOS built-in tools. In this case a few command line utilities.


## The "Who" {#the-who}


### find the USB device {#find-the-usb-device}

```sh
diskutil list
```

The output on my machine is (I removed some devices to shorten the code block):

```text
/dev/disk0 (internal, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *500.3 GB   disk0
   1:             Apple_APFS_ISC Container disk1         524.3 MB   disk0s1
   2:                 Apple_APFS Container disk3         494.4 GB   disk0s2
   3:        Apple_APFS_Recovery Container disk2         5.4 GB     disk0s3

/dev/disk4 (external, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *61.5 GB    disk4
   1:                        EFI EFI                     209.7 MB   disk4s1
   2:                  Apple_HFS 64gb                    61.2 GB    disk4s2
```

Here the thumb drive is `/dev/disk4`. This might of course differ for you.


### Create an empty Partition of the whole disk {#create-an-empty-partition-of-the-whole-disk}

If not used with caution all of the following steps are quite dangerous and might lead to you loosing your files.

<style>.orange {color: #bf616a;font-weight: bold;}</style>

<div class="orange">

-   The next steps will erase everything on the disk.
-   Make sure to use the right disk.

</div>


#### Unmount the disk: {#unmount-the-disk}

```sh
diskutil unmountDisk /dev/disk4
```


#### Wipe the disk: {#wipe-the-disk}

```sh
sudo diskutil partitionDisk /dev/disk4 1 MBR "Free Space" "%noformat%" 100%
```


### Burn the ISO onto the drive {#burn-the-iso-onto-the-drive}

The [`dd`](https://manpage.me/?q=dd) command being one of the most destructive commands on your Mac should be used carefully.

<style>.orange {color: #bf616a;font-weight: bold;}</style>

<div class="orange">

Double check the command and the right disk selection.

</div>

```sh
sudo dd bs=8m if=$HOME/Downloads/ubuntu-20.04-desktop-amd64.iso of=/dev/rdisk4
```

The `r` in `/dev/rdisk4` is not a typo. This is how it is supposed to be.

Also in this case `ubuntu-20.04-desktop-amd64.iso` is just an example. the `if` (input file) can of course be any other ISO from any other path.

Once you get sent back to the prompt the ISO is burnt onto the drive and in the case of an OS also bootable.


#### Bonus: {#bonus}

You can check the progress in another Terminal instance.

```sh
sudo kill -INFO $(pgrep ^dd$)
```