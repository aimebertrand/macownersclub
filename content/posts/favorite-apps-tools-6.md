+++
title = "Favorite Apps & Tools #6 - Alfred"
author = ["Aimé Bertrand"]
date = 2021-06-24T00:00:00+02:00
lastmod = 2021-06-24T21:28:02+02:00
tags = ["tools", "software"]
categories = ["apps"]
draft = false
+++

`Spotlight` is a fantastic idea/concept first introduced with Mac OS X Tiger. With the first release it was mildly revolutionary and immensely useful. But as time passed the Macs and macOS got faster, much faster. Unfortunately Spotlight did not keep up. It seems that Apple in the end treated it like a red headed stepchild.

{{< figure src="/images/alfred_pure.png" width="800px" >}}

Enter the success of third party tools like [LaunchBar](https://obdev.at/products/launchbar/index.html), [Quicksilver](https://qsapp.com/), [Raycast](https://raycast.com/) and of course the app of today's post, [Alfred](https://www.alfredapp.com/). Not only did they get better treatment and development, but they added a bunch more useful functionality.

However I argue that so far Alfred has won the race. Let me explain!


## What does it do exactly? {#what-does-it-do-exactly}

Alfred is a self-professed

> Award-winning app for macOS which boosts your efficiency with hotkeys, keywords, text expansion and more.

In essence Alfred is a launcher. With just a keystroke – Spotlight's `⌘ + space` in my case – you call up the interface and type any assigned keywords for the function you want. **From everywhere in macOS!** With that you can launch among other things:

-   Searches
    -   locally
    -   in search engines of all kind
    -   etc.
-   Applications
-   Scripts
-   Automation tasks
-   Much more

{{< figure src="/images/alfred_duck.png" width="800px" >}}

Alfred is not just limited to being a launcher though. With a licence for the [powerpack](https://www.alfredapp.com/powerpack/) it also has a plugin system.

The community has written a whole host of so called `workflows`. These are all manner of automation tasks that can be plugged into Alfred. First source besides countless other repositories is [Packal](http://www.packal.org/). Alfred's own online repositories, where you can find all sort of workflows. Or just the [curated list](https://www.alfredapp.com/workflows/).

-   Dictionaries
-   Script runners
-   Installing Apps
-   System settings
-   Password management
-   Application control
-   Server connection

{{< figure src="/images/alfred_brew.png" width="800px" >}}

This is but a tiny selection of the immense number of workflows.


## How do I use it? {#how-do-i-use-it}

By far the Automation I use the most is launching applications. The speed is fantastic and it feels like the apps start as fast as I can think. This of course is built-in. No need for the `powerpack`.

My second favorite Automation is the [translation workflow](https://github.com/dennis-tra/alfred-dict.cc-workflow) using `dict.cc`. Supports most languages that I use and is – again – quite fast.

{{< figure src="/images/alfred_dict.png" width="800px" >}}

Another workflow that I use quite frequently is the [pass worklow](https://github.com/CGenie/alfred-pass). It is a front-end to my password store using the [pass](https://www.passwordstore.org/) command. This gets a password you search for in your clipboard in no time - with a timeout to delete it.

And here are some of the other workflows for you to checkout:

{{< figure src="/images/alfred_light.png" width="800px" >}}

-   [Toggle Theme](https://github.com/mermaid/alfred-dark-mode-toggle): Change the theme of macOS.
-   [xkcd](https://github.com/zjn0505/xkcd-alfred): Search xkcd comics and what if articles
-   [Homebrew & Cask for Alfred](https://github.com/fniephaus/alfred-homebrew/): Control Homebrew & Cask with Alfred.
-   [Alfred Gitlab](https://github.com/lukewaite/alfred-gitlab): Quickly navigate to GitLab projects.
-   [Kill Process](https://github.com/nathangreenstein/alfred-process-killer): Kill misbehaving processes.


## What is the secret sauce? {#what-is-the-secret-sauce}

Alfred takes the cake because it is fast. **In fact I think that the bottleneck is your speed on the keyboard**. This is a god-sent compared to the snail speed of spotlight. This is alone is really worth it to me.

{{< figure src="/images/alfred_settings.png" width="800px" >}}

Alfred achieves this by categorizing the search. For instance the search without keywords is confined in my case to applications, preferences and contacts. To search for files on disk I would type `⌘ + space` then `'` or just as an easier version `⌘ + space` then `space`. All of this can be controlled quite minutely in the preferences.
