+++
title = "Org-roam v2 doesn't hurt"
author = ["Aimé Bertrand"]
date = 2021-07-20T00:00:00+02:00
lastmod = 2021-07-20T23:51:03+02:00
tags = ["org", "org-roam", "zettelkasten"]
categories = ["emacs"]
draft = false
+++

## Why? {#why}

Today was one of the days I decided to upgrade all my installed Emacs packages via `list-packages`.

The upgrade to Org-roam v2 should have not surprised me – it is and has been widely talked about in the community the developer – but it did `¯\_(ツ)_/¯`. This meant of course the adjustment of my configuration to meet the requirements of the new version.

Having followed the upfront chatter about the "breaking upgrade" I was afraid to have some work cut out for me. But in hind side I realize that this was not the case at all. Most time spent with the migration is this post really.

I will not discus the changes or the new feature of Org-roam v2. This is just a fairly fast How-To to get a working configuration before diving in deeper. There are some links to resources further down however.

So if I can manage then it should be a breeze for you. The tl;dr version of my changes can be found in this [commit](https://gitlab.com/aimebertrand/dotfiles/-/commit/9bfe4dd0d5303fdd7177c5655bd3dadd3b7a0b3f). Let us get into some details though.


## How? {#how}

There are really just 3 steps in my humble opinion that you should take care of. <span style="color:#d08770;font-weight:bold;">But – for the love of god – backup your Org-roam directory first!</span>


### 1. Adjust your configuration {#1-dot-adjust-your-configuration}

Note that the `org-roam.db` can be separated from the `org-roam-directory`. I however choose to put the db in the same location.

```emacs-lisp
(with-eval-after-load 'org
  (progn
    (setq org-roam-v2-ack t) ;; acknowledge upgrade and remove warning at startup
    (require 'org-roam nil t)
    (setq org-roam-directory
          (file-truename "/path/to/org-roam-directory"))
    (setq org-roam-db-location
          (concat org-roam-directory "org-roam.db"))
    (org-roam-setup)))
```

`org-roam-setup` replaces `org-roam-mode`. This sets up Org-roam and initializes it at Emacs startup.

In case you are using `use-package` like me the following configuration should do the trick.

```emacs-lisp
(use-package org-roam
  :ensure t
  :after org
  :init
  (setq org-roam-v2-ack t) ;; acknowledge upgrade and remove warning at startup
  :config
  (setq org-roam-directory
        (file-truename "/path/to/org-roam-directory"))
  (setq org-roam-db-location
        (concat org-roam-directory "org-roam.db"))
  (org-roam-setup))
```


### 2. Change keybindings to the new functions {#2-dot-change-keybindings-to-the-new-functions}

I personally truly only use 3 Keybindings for Org-roam. You should of course change the bindings to your liking.

```emacs-lisp
;; insert a link to an org-roam file – `org-roam-insert' in v1:
(global-set-key (kbd "C-M-i") 'org-roam-node-insert)
;; open a file in org-roam – `org-roam-find-file' in v1:
(global-set-key (kbd "C-M-o") 'org-roam-node-find)
;; open backlinks buffer – `org-roam' in v1:
(global-set-key (kbd "C-M-r") 'org-roam-buffer-toggle)
```


### 3. Migrate your Org-roam {#3-dot-migrate-your-org-roam}

-   Eval your new configuration. I just restart Emacs.
-   Run the provided migration wizard `M-x org-roam-migrate-wizard`.
-   For good measure sync changes to your DB – `M-x org-roam-db-sync`.


## Conclusion {#conclusion}

Admittedly I don't handle very big DBs with Org-roam (max 145 files). I choose to have multiple directories rather than a huge `org-roam-directory`. For that your results may vary, but I am fairly convinced that – as mentioned further up – it will not hurt a lot.


## Resources {#resources}

-   [Hitchhiker's Rough Guide to Org roam V2](https://github.com/org-roam/org-roam/wiki/Hitchhiker's-Rough-Guide-to-Org-roam-V2) - Guide by the developer.
-   [Migrating from Org-roam v1](https://www.orgroam.com/manual.html#Migrating-from-Org%5F002droam-v1) - Chapter in the manual.
-   [Org-roam version 2 - Should one upgrade?](https://www.youtube.com/watch?v=FRTaXVcQLNc) - Intro video by Emacs-Elements.
-   [The Org-roam v2 Great Migration](https://org-roam.discourse.group/t/the-org-roam-v2-great-migration/1505) - Discussion on the Org-roam homepage.
