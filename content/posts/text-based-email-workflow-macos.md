+++
title = "Text based email workflow on macOS"
author = ["Aimé Bertrand"]
date = 2022-04-30T00:00:00+02:00
lastmod = 2022-04-30T22:08:13+02:00
tags = ["email", "text", "macos", "software"]
categories = ["macos"]
draft = false
+++

## But why though? {#but-why-though}

macOS has arguably the best looking, most succinct and most user friendly GUI in all the OS commonly used on personal computers.

With this comes a bunch of fantastic GUI based tools for everything – including a lot for emailing.

The following list is but a selection of some.

-   Mail.app
-   [Spark](https://apps.apple.com/de/app/spark-email-app-by-readdle/id1176895641?l=en&mt=12)
-   [Microsoft Outlook](https://apps.apple.com/de/app/microsoft-outlook/id985367838?l=en&mt=12)
-   [Canary Mail App](https://apps.apple.com/de/app/canary-mail-app/id1236045954?l=en&mt=12)
-   [Airmail 5](https://apps.apple.com/de/app/airmail-5/id918858936?l=en&mt=12)

Just with this selection, I am quite sure a lot of people would find their match. Personally I happily used the default macOS `Mail.app`. It did everything, that I needed. It is well integrated into the OS and [iCloud](https://www.icloud.com) of course. Together with Calendar.app and Contacts.app this quite a potent and well oiled machinery.

Then why did I switch to a text based workflow?

A few articles ago I wrote about my [obsession with Emacs](https://macowners.club/posts/favorite-apps-tools-2/). Since I started using Emacs, I have been down the rabbit hole of migrating most of my workflows into it. Emacs is a fantastic text based – for lack of a better word – environment.

Breaking things to their basic nature takes away the distraction and it does not get any more basic than plain text.

Granted, I noticed that the appeal is not just the text based nature of the application, but also the focus on fuzzy searching/finding. In Emacs one can do this on any imaginable type of list.

Most of my navigation is initiated by a [fuzzy completion interface](https://macowners.club/posts/from-ivy-to-vertico/). I accomplish this with a whole host of applications, programs and add-ons.

-   [Alfred](https://macowners.club/posts/favorite-apps-tools-6/)
-   [Vertico &amp; consult](https://macowners.club/posts/from-ivy-to-vertico/) in Emacs
-   [fzf](https://macowners.club/posts/favorite-apps-tools-4/) and more...

<style>.orange {color: #d08770;}</style>

<div class="orange">

So I handle emails in plain text. But how?

</div>


## The Toolchain {#the-toolchain}

First things first. Let's take a look at the tools and programs that I user to achieve that.


### Emacs &amp; mu4e {#emacs-and-mu4e}

As mentioned before I already wrote up my setup to handle emails with Emacs. This is extensively illustrated in [this article](https://macowners.club/posts/email-emacs-mu4e-macos/).

{{< figure src="/images/emacs_mu4e.png" width="800px" >}}


### NeoMutt {#neomutt}

For the rare cases I don't have Emacs running and I just want a glance into my mailboxes I can quickly launch NeoMutt on the command line. This also has the added benefit of having a backup client for my email.

Both Mu4e and NeoMutt use `mbsync` and the same email directory structure located in `~/.maildir`. Again demonstrated in the aforementioned article.

[NeoMutt](https://neomutt.org), which to my knowledge is based [Mutt](http://www.mutt.org/), is a wonderful email client for the command line. It is quite customizable, both in behaviour (keybindings, email handling) and look &amp; feel.

It is however quite intense to configure. Luckily [the guide](https://neomutt.org/guide/) online is detailed enough.

In this post I will not go through the details on how to configure NeoMutt. I will rather layout some of the special custom settings that might not be documented in the guide.

In my case the configuration files are in the `~/.mutt` directory as documented [here](https://neomutt.org/guide/configuration#1-2-%C2%A0location-of-user-config-files).

```sh
├── accounts           # configuration for every account in a separate file
│   ├── aimebertrand
│   ├── icloud
│   ├── mocarchive
│   └── moclub
├── bindings           # file containing the keybindings
├── colors             # theming
├── muttrc             # main configuration
└── signatures         # signature are kept separate as well
    ├── aimebertrand-sign
    ├── icloud-sign
    ├── mocarchive-sign
    └── moclub-sign
```

Once configured, the result looks like this:

{{< figure src="/images/neomutt.png" width="800px" >}}

Some of the custom settings, that you will not find in the guide include a mailbox switcher/selector using [fzf](https://macowners.club/posts/favorite-apps-tools-4/). I use a script which is then bound to a key in the binding file.

**bash script `muttfzf.sh`:**

```sh
#!/bin/bash

match_folder() {
    find $HOME/.maildir -name '*' -type d \
-mindepth 2 -maxdepth 2 |\
grep -v '.git' | grep -v '\/mu' |\
        fzf --reverse
}

folder=$(match_folder)

echo "push 'c$folder<enter>'"
```

**Keybinding:**

```sh
macro index,pager J ":source ~/.dotfiles/bin/muttfzf.sh|<enter>"
```


### Notmuch {#notmuch}

[Notmuch](https://notmuchmail.org) is an email indexing and search tool – just like [mu/mu4e](https://www.djcbsoftware.nl/code/mu/mu4e.html) – based on [Xapian](https://xapian.org). This provides a way to index my emails from the `~/.maildir`. However separate to the mu xapian database.

This is useful because I can use the terminal to quickly fuzzy search for an email and display it quite fast on the command line. To achieve I use a custom function, that chains notmuch, fzf, mu &amp; nvim.

{{< figure src="/images/notmuch.png" width="800px" >}}

{{< figure src="/images/notmuch1.png" width="800px" >}}

```sh
function nmail() {
    set -o pipefail
    mail=$(notmuch show --format=text \
                   $(notmuch search date:1970..2021 | \
                         fzf --reverse --preview \
                             "echo {} | cut -d' ' -f1 | cut -d':' -f2 | \
xargs notmuch show --format=text | \
grep 'message{' | head -n 1 | cut -d':' -f6,7 | \
sed 's/\ /\\\ /g' | xargs mu view" | \
                         cut -d' ' -f1 | cut -d':' -f2) | \
               grep 'message{' | head -n 1 | cut -d':' -f6,7 | \
               sed 's/\ /\\\ /g' | xargs mu view) && \
        echo $mail | nvim -c 'set ft=mail' -c 'nmap q :q!<CR>'
}
```

This is beautifully convenient and fast, when I just want to quickly fuzzy match a subject in all of my emails and take a peak of the content. And I don't need to open a fully fledged client and search.


## Conclusion {#conclusion}

Looking at this from a purely sober perspective it looks like a lot of hoops to go through. And granted the overhead is quite challenging. But the result speaks for itself. the convenience of finding emails at the speed of thought is a great one. Plus I have the added benefit of having all my email archive stored in a format that is readable by almost any sort of digital device.