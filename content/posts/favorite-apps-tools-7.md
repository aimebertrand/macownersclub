+++
title = "Favorite Apps & Tools #7 - Affinity Suite"
author = ["Aimé Bertrand"]
date = 2021-07-03T00:00:00+02:00
lastmod = 2021-07-04T03:01:35+02:00
tags = ["tools", "software"]
categories = ["apps"]
draft = false
+++

## Reason {#reason}

The Adobe Creative Cloud has a set of fantastic tools and is probably the go to for most professional creative lots out there.

However they are mind-bogglingly expensive for normal people who just want to edit their digital creations. Plus the subscription model is still an immense caveat for most people including me. In comes the [Affinity suite](https://affinity.serif.com/en-us/).


## The Applications {#the-applications}

It is a set of three applications for creatives. According to the vendor, [Serif](https://www.serif.com/en-us/):

> Award-winning creative graphics software

Now I am by no means a designer, but I do fairly often need a digital tool for either editing photos, light DTP and graphics for family and friends and other a bunch of other stuff. I have also managed the IT for a bunch of creative agencies using mostly the Adobe tools in the past.

Thus I am reasonably certain that the applications are more than up to the task for the "normal" user. There is even a serious argument to be made that they are capable for a lot of professional users. I suspect that Adobe's firm grasp is not due to the chops but rather because of the propensity of most to sticking to the old.

{{< figure src="/images/affinity_photo_app.png" caption="Figure 1: Affinity Photo – This is the substitution for Photoshop." width="800px" >}}

Not only is the Affinity suite more able to handle my needs for graphic work at a much lower price, but they do this in style. I find that they fit much more to the design language of macOS.

{{< figure src="/images/affinity_designer_app.png" caption="Figure 2: Affinity Designer – Does Illustrator's job." width="800px" >}}

The workflow is surprisingly similar to the Adobe products – actually down to the layout of the application. Plus there are quite extensive tutorials on [the Affinity sites](https://affinity.serif.com/en-gb/learn/).

{{< figure src="/images/affinity_publisher_app.png" caption="Figure 3: Affinity Publisher – Use this instead of InDesign." width="800px" >}}

<p style="color:#d08770;"><span style="font-weight:bold;">I have not told you the best part yet. It costs as of the writing of this articles roughly 160€ and you keep it permanently in contrast to Adobe's subscription.</span></p>


## Conclusion {#conclusion}

I have tried to come up with a proper reason for not using these applications instead the Adobe suite if you do not absolutely need it in your job. I could not do it.

It is an entirely different story for many enterprise environments. These tend to have a rigid workflow with the Adobe products deeply integrated in the processes. Changing the tooling here is mostly a costly enterprise.
