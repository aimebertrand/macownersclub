+++
title = "Finder context menu item with Automator"
author = ["Aimé Bertrand"]
date = 2022-05-01T00:00:00+02:00
lastmod = 2022-05-01T03:18:48+02:00
tags = ["macos", "finder", "context-menu", "automator"]
categories = ["macos"]
draft = false
+++

I want to create a context menu item in **Finder.app** to open selected items with Emacs. So why not document it for others to see. Might be instructive for some as well.

-   Open the `Automator.app`.
-   Create a new document of the type **Quick Action**.

{{< figure src="/images/context-menu-automator-1.png" width="800px" >}}

-   On the right side of the window select `files or folders` and `Finder.app`.
-   On the left side select `Run Shell Script` action and drag it onto the canvas to build the workflow.

{{< figure src="/images/context-menu-automator-2.png" width="800px" >}}

-   In the `Run Shell Script` step change the `Pass input` to `as arguments`.
-   With the following script one can select either one or more files/directories.

<!--listend-->

```sh
for f in "$@"
do
    open -a Emacs "$f"
done

exit 0
```

{{< figure src="/images/context-menu-automator-3.png" width="800px" >}}

-   Go to **File -&gt; Save** in the menu to save the workflow. Choose a name which will appear in the context menu.

{{< figure src="/images/context-menu-automator-4.png" width="800px" >}}

-   **That is it.** You will now find your new item in the context menu.

{{< figure src="/images/context-menu-automator-5.png" width="400px" >}}