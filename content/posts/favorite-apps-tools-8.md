+++
title = "Favorite Apps & Tools #8 - Docker"
author = ["Aimé Bertrand"]
date = 2022-01-04T00:00:00+01:00
lastmod = 2022-01-05T00:56:36+01:00
tags = ["apps", "tools", "software"]
categories = ["macos"]
draft = false
+++

## Tools {#tools}

In contrast to the use cases mentioned in [the official documentation](https://docs.docker.com/get-started/overview/) my private use of Docker is much more mundane. I mostly need it to extend my tooling.

There is not much missing on my macOS machines to accommodate all of my needs. But in some rare cases I need to have access to some GNU/Linux exclusive software.

{{< figure src="/images/docker.png" width="150px" >}}

On those occasional instances I have grown to rely on docker.

-   Fire up a docker container with the [appropriate image](https://hub.docker.com/search?type=image).
-   Install the needed software in case they are not built in.
-   Run the task(s) and destroy the container as appropriate.

&lt;span style="color:#d08770;"&gt;This is probably better illustrated with an example.&lt;/span&gt;


## This is not a how to {#this-is-not-a-how-to}

I recently got into a pickle, where I needed to delete A LOT of duplicate mails in my [maildir directory](https://macowners.club/posts/email-emacs-mu4e-macos/).

After an extensive search on the internet, I decided that [this wonderful post](http://kremvax.net/howto/mail-duplicates.html) would help me the most.

The only hiccup was that the how-to uses `reformail` (included in `maildrop`) which was not available on macOS.

Well... Docker to the rescue.

-   Fired up an Ubuntu image with mapped volumes to my maildir directory.
-   Installed maildrop.
-   Performed the steps as mentioned

And that was that.


## Conclusion {#conclusion}

One cannot deny the usefulness of the container concept. Not just for Devs and/DevOps, but for any type of user.

At work Docker is as much a necessity as my machine itself. But I like Docker much more at home for these one-off tasks – similar to the example above.

But what about Windows software? Just... No.