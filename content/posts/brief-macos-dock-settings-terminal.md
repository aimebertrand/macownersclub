+++
title = "Brief #3 - macOS Dock Settings on the Terminal"
author = ["Aimé Bertrand"]
date = 2022-11-26T00:00:00+01:00
lastmod = 2022-11-27T00:59:38+01:00
tags = ["dock", "macos", "terminal"]
categories = ["macos"]
draft = false
+++

## Intro {#intro}

All – probably more – settings for the Dock found in the `System Settings.app` in macOS can be adjusted using the command line. This is particularly helpful for automation of course.

IMHO the following selection of commands is more than enough for most settings one needs.

{{< figure src="/images/dock-config.png" width="800px" >}}


## Commands {#commands}

<style>.orange {color: #d08770;}</style>

<div class="orange">

**Note:**
: Run `killall Dock` after any of the commands for the changes to take effect.

</div>


### Dock's Position {#dock-s-position}

```sh
# Possible options 'bottom', 'left' or 'right'
defaults write com.apple.dock orientation bottom
```


### Dock size {#dock-size}

```sh
# Integer... In my case 32 is the "magic" number
defaults write com.apple.dock tilesize -int 32
```


### Dock magnification {#dock-magnification}

```sh
# Possible options 'yes' or 'no'
defaults write com.apple.dock magnification -boolean yes
```


### Auto hide the Dock {#auto-hide-the-dock}

```sh
# Possible options 'yes' or 'no'
defaults write com.apple.dock autohide -boolean yes
```


### Auto hide delay {#auto-hide-delay}

```sh
# Reasonable values between 0 and 0.9 (ms). – Snaller number = faster appearance
defaults write com.apple.dock autohide-time-modifier -float 0
defaults write com.apple.dock autohide-delay -float 0
```


### Reset to factory default {#reset-to-factory-default}

```sh
# This removes all the settings. macOS recreates them with factory defaults
defaults delete com.apple.dock
```


## Bonus {#bonus}

macOS does not have an elegant option for managing the items in the dock.

However there is a FOSS tool by the name [dockutil](https://github.com/kcrawford/dockutil) for that:

> dockutil is a command line utility for managing macOS dock items. It is currently written in Swift.
