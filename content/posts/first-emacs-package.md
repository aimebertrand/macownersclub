+++
title = "My first Emacs package on Melpa"
author = ["Aimé Bertrand"]
date = 2021-06-12T00:00:00+02:00
lastmod = 2021-06-12T16:32:56+02:00
tags = ["emacs", "package", "elisp"]
categories = ["emacs"]
draft = false
+++

## Why? {#why}

I am greatly fond of Emacs as a tool, as evident by [this post](https://macowners.club/posts/favorite-apps-tools-2/) or [this ones](https://macowners.club/categories/emacs/). In fact it is my favorite application/utility on any computer – by far.

{{< figure src="/images/timu-spacegrey-dark.png" width="800px" >}}

Because of this I want to contribute to the available packages. Now I am no wiz when it comes to programming, but [I am tying to learn](http://localhost:1313/categories/python/).

> So where to start?

Make a package
: In my case it is a theme. Themes are easy to create. Plus I used quite a bit of boilerplate from other packages (built-in themes, [doom themes](https://github.com/hlissner/emacs-doom-themes), etc). Most of the work was bringing the code together and of course choosing colors, that I find pleasant and wholesome.

Deploy to a public Repository
: My choice was [Melpa](https://melpa.org/#/), since it seams to be the most popular in the community. Also there are much fewer hoops to jumps through compared to something like the official [Elpa](https://elpa.gnu.org/).


## What? {#what}

The theme that I came up with is called `timu-spacegrey-theme`. A colorful theme with orange as the leading color with a **dark** and a **light** flavour. It is inspired by Gadzhi Kharkharov’s [Spacegray](https://kkga.github.io/spacegray/) theme for Sublime Text.

{{< figure src="https://melpa.org/packages/timu-spacegrey-theme-badge.svg" >}}

As mentioned above themes are reasonably easy to create and deploy as a package. But this was not the only reason to create a theme as my first package.

I wanted to scratch my itch as well. I have been trying out a whole host of themes, but none pleased me to the fullest. Granted mine is still a work in progress, but I can fix stuff myself and contribute at the same time.

The theme has been online for a few days now and it seams, that quite a few people like it. At least judging by the downloads.


### well this is it {#well-this-is-it}

-   Code repository [on GitLab](https://gitlab.com/aimebertrand/timu-spacegrey-theme)
-   Package info [on Melpa](https://melpa.org/#/timu-spacegrey-theme)

{{< figure src="/images/timu-spacegrey-1.png" width="800px" >}}


### Installation {#installation}


#### Manual installation {#manual-installation}

1.  Download the [timu-spacegrey-theme.el](https://gitlab.com/aimebertrand/timu-spacegrey-theme/-/raw/main/timu-spacegrey-theme.el?inline=false) file and add it to your `custom-load-path`.
2.  In your `~/.emacs.d/init.el` or `~/.emacs`:

<!--listend-->

```emacs-lisp
(load-theme 'timu-spacegrey t)
```


#### From [Melpa](https://melpa.org/#/timu-spacegrey-theme) {#from-melpa}

1.  `M-x package-instal <RET> timu-spacegrey-theme.el <RET>`.
2.  In your `~/.emacs.d/init.el` or `~/.emacs`:

<!--listend-->

```emacs-lisp
(load-theme 'timu-spacegrey t)
```


#### With [use-package](https://github.com/jwiegley/use-package) {#with-use-package}

```emacs-lisp
(use-package timu-spacegrey-theme
  :ensure t
  :config
  (load-theme 'timu-spacegrey t))
```

{{< figure src="/images/timu-spacegrey-2.png" width="800px" >}}


### Configuration {#configuration}


#### Light and dark flavour {#light-and-dark-flavour}

There is a light version included as well. By default the theme is **dark**.
To setup the **light** flavour add the following to your `~/.emacs.d/init.el` or `~/.emacs`:

```emacs-lisp
(setq timu-spacegrey-flavour "light")
```


#### Auto switching the flavour on macOS {#auto-switching-the-flavour-on-macos}

You can switch the **light** and **dark** modes on macOS with the system automatically as well. Unfortunately I don't know how this would work on the many Linux Distros.

```emacs-lisp
(add-hook 'ns-system-appearance-change-functions
          #'(lambda (appearance)
              (mapc #'disable-theme custom-enabled-themes)
              (pcase appearance
                ('light (progn (setq timu-spacegrey-flavour "light")
                               ;; hack for the titlebar text:
                               (add-to-list 'default-frame-alist
                                            '(ns-appearance . dark))
                               (load-theme 'timu-spacegrey t)))
                ('dark (progn (setq timu-spacegrey-flavour "dark")
                              (load-theme 'timu-spacegrey t))))))
```
