+++
title = "Custom Emacs functions No. 3 - Dired"
author = ["Aimé Bertrand"]
date = 2023-03-19T00:00:00+01:00
lastmod = 2023-07-26T21:35:48+02:00
tags = ["emacs", "commands", "functions", "dired"]
categories = ["emacs"]
draft = false
+++

## File Management {#file-management}

There should now be a trend in this series for custom functions (see the last posts for [baseline](https://macowners.club/posts/custom-functions-1-baseline/) &amp; [modules](https://macowners.club/posts/custom-functions-2-modules/) functions). I like things to work my way.

This is of course a reasonably easy thing to do using Emacs. With it being extensible and stuff...

It is no different with file management using Dired.


## Dired custom functions {#dired-custom-functions}


### timu-dired-up-directory {#timu-dired-up-directory}

Dired by defaults keeps the old Dired buffer when navigating on the file systems. This causes Emacs to "collect" a bunch of opened Dired buffers. I and my mild OCD do not like this one bit.

This functions navigates up a directory in dire by using `find-alternate-file` which kills the "previous buffer".

```emacs-lisp
(defun timu-dired-up-directory ()
  "Go up a directory in `dired'."
  (interactive)
  (find-alternate-file ".."))
```


### timu-dired-open-all-in-emacs {#timu-dired-open-all-in-emacs}

This functions forces the marked files or the file at point to be opened inside of Emacs.

```emacs-lisp
(defun timu-dired-open-all-in-emacs (&optional file)
  "Open the current FILE or Dired marked files in Emacs."
  (interactive)
  (let (doIt (myFileList
              (cond
               ((eq major-mode 'dired-mode)
                (dired-get-marked-files))
               ((not file) (list (buffer-file-name)))
               (file (list file)))))
    (setq doIt (if (<= (length myFileList) 30) t
                 (y-or-n-p "Open more than 30 files? ")))
    (mapc (lambda (fPath)
            (let ((process-connection-type nil))
              (start-process "" nil "emacsclient" fPath))) myFileList)))
```


### timu-dired-open-in-external-app {#timu-dired-open-in-external-app}

Simple function to open a marked files or the file at point in the default macOS App for the file type.

```emacs-lisp
(defun timu-dired-open-in-external-app (&optional file)
  "Open the current FILE or Dired marked files in external app.
The app is chosen from your OS's preference.
Credit: http://xahlee.info/emacs/emacs/emacs_dired_open_file_in_ext_apps.html."
  (interactive)
  (let (doIt (myFileList
              (cond
               ((eq major-mode 'dired-mode)
                (dired-get-marked-files))
               ((not file) (list (buffer-file-name)))
               (file (list file)))))
    (setq doIt (if (<= (length myFileList) 30) t
                 (y-or-n-p "Open more than 30 files? ")))
    (when doIt
      (cond
       ((string-equal system-type "windows-nt")
        (mapc (lambda (fPath)
                (w32-shell-execute
                 "open" (replace-regexp-in-string "/" "\\" fPath t t)))
              myFileList))
       ((string-equal system-type "darwin")
        (mapc (lambda (fPath)
                (shell-command (format "open \"%s\"" fPath)))
              myFileList))
       ((string-equal system-type "gnu/linux")
        (mapc (lambda (fPath)
                (let ((process-connection-type nil))
                  (start-process "" nil "xdg-open" fPath))) myFileList))))))
```


### timu-dired-shell-open-dir {#timu-dired-shell-open-dir}

This one opens the current directory in the Finder.app on macOS.

```emacs-lisp
(defun timu-dired-shell-open-dir ()
  "Open current directory at point with shell command \"open\".
This will open \"Finder.app\" at current location."
  (interactive)
  (timu-baseline-async-shell-command-no-window "open ./" ))
```


### timu-dired-shell-quicklook {#timu-dired-shell-quicklook}

The function uses macOS [Quick Look](https://support.apple.com/en-gb/guide/mac-help/mh14119/mac) feature to preview the file at point. I use this one all the time.

```emacs-lisp
(defun timu-dired-shell-quicklook ()
  "Open the files at point with shell command \"qlmanage\".
This will display a Quicklook of the file at point in macOS."
  (interactive)
  (setq file (dired-get-file-for-visit))
  (timu-baseline-async-shell-command-no-window
   (concat "qlmanage -p " (shell-quote-argument file) " > /dev/null 2>&1")))
```


### timu-dired-copy-path-at-point {#timu-dired-copy-path-at-point}

Honestly do not use this one a lot. However it is quite useful to have in those cases. It is simple. It copies the full path of the file at point.

```emacs-lisp
(defun timu-dired-copy-path-at-point ()
  "Copy the full path of the at `point' to the `kill-ring'.
Credit: https://emacs.stackexchange.com/a/36851/30874"
  (interactive)
  (dired-copy-filename-as-kill 0))
```


### timu-dired-search-and-enter {#timu-dired-search-and-enter}

`consult-line` is a wonderful command to search the current buffer. I use it to search for a directory in the Dired buffer and enter it with `dired-find-alternate-file`.

```emacs-lisp
(defun timu-dired-search-and-enter ()
  "Search file or directory with `consult-line' and then visit it."
  (interactive)
  (consult-line)
  (dired-find-alternate-file))
```
