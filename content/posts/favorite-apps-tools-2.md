+++
title = "Favorite Apps & Tools #2 - Emacs"
author = ["Aimé Bertrand"]
date = 2021-05-15T00:00:00+02:00
lastmod = 2021-05-21T00:12:47+02:00
tags = ["tools", "software"]
categories = ["apps"]
draft = false
+++

## Warning {#warning}

This is gonna be a long one! Totally worth it though.


## Meeting Emacs {#meeting-emacs}

My first contact was when I was looking for an editor/IDE like most people learning to code. Now before everyone screams, that [Emacs](https://www.gnu.org/software/emacs/) is not an editor or an IDE for that matter... I know that. You don't believe me? Read on!

For some reason that I do not really recall my decision fell on Emacs. So started the internet rabbit hole of finding out more about it. The more I looked into it the more I got convinced to have made the right decision. And finally I was hooked and got obsessed.


## The App {#the-app}

{{< figure src="/images/emacs_full.png" width="800px" >}}

People way smarter than I am have tried to describe Emacs and did not get far. So I thought to myself: "How hard can it be?". Obscenely hard. Hence me confining myself to saying, what I do with it rather that attempting to do the impossible.

We could go by the short description of the creators of course:

> Emacs is the advanced, extensible, customizable, self-documenting editor.

I have to mention some stuff to get this going though.

The Power of Emacs indeed lies in the extensible, customizable, self-documenting, nature of the application. In essence Emacs is an editor and a lisp — more correct `Emacs-Lisp` — interpreter. It has an integrated package manager, [package.el](https://wikemacs.org/wiki/Package.el) and the Free Software Foundation maintains a Package Repository, [GNU Emacs Lisp Package Archive](https://elpa.gnu.org/) (`ELPA`).

Also because of a vivid community of Emacs users, developers and contributors there are some alternatives to those. [Straight.el](https://github.com/raxod502/straight.el) and [el-get](https://github.com/dimitri/el-get) to name some of the alternative package managers. Plus `MELPA`, the [Milkypostman’s Emacs Lisp Package Archive](https://melpa.org) is a great example of community maintained Packages repos. One can not only get packages from official and unofficial repos, but also directly from git repos.

There are few very important concepts though — at least for me. It is rather imperative to mention these before we go on for this post not to be just some rambling. Well, there is still an imminent danger for it to be.

**First**
: ... concept is the `keyboard focused` navigation of Emacs. each function can be "attached" to a keybinding, meaning a combination of key presses. All, I repeat all of these bindings can be modified by the user, and I heavily make use of that.

<!--listend-->

**Second**
: ... concept is the navigation through `fuzzy matching/finding`. This means that any type of list including functions can be called, fuzzy matched and the result is then acted on or applied.

{{< figure src="/images/emacs_fuzzy_matching.gif" width="800px" >}}

**Third**
: ... concept is the `buffer` system. Essentially this means, that any display of content is in a buffer. This is like a surface on which files, directory listings, images, and so on are displayed on. In Photoshop or any modern image editor, this would be called "layer". Meaning that Emacs actually stacks those buffers either on top/behind of each other or side by side or all together.

<!--listend-->

**Fourth**
: ... is the concept of `minor modes` and `major modes`. This concept is incredibly hard to explain, but I will give it a shot. A major mode mostly corresponds to the buffer type (meaning, which kind of files is opened, which package has been called or which content is being displayed). Depending on the active major mode, one has a particular set on keybindings and functions front and center. For each buffer only one major mode is active. A buffer can have multiple minor modes though. These come with their own Keybindings and functions as well. A much better explanation is in [documentation](https://www.gnu.org/software/emacs/manual/html%5Fnode/emacs/Modes.html).

All of the above makes Emacs a fantastic text based environment for all sorts of tasks one does on a computer. You can choose to operate Emacs on the CLI or in GUI form, which brings another layer of capabilities. Like working with images, graphs, PDFs and much more.

As a bonus. You can make Emacs look exactly the way you want it to look like. I mean that. The only limit are the for corners of the window — "frame" in Emacs-Speak.

> So, what do I use Emacs for?


## TL;DR {#tl-dr}

-   Writing
-   IDE
-   Git Client
-   Email Client
-   RSS Reader
-   File Manager
-   PDF Tools
-   Docker Client
-   Fun
-   GTD
-   Blogging

The above list is by far not exhaustive. Not even for what I do. Just imagine how fantastic the possibilities are in the hands of more versed hands.


## Writing {#writing}

I am not an author in any meaning of the word. Any form of prose, that I write has to do with this blog, documentations at work, some private notes, journaling and other minor stuff. But whenever I do, I do it in Emacs. If you want to see a proper author talking about writing using Emacs, there is a fantastic [talk by Jay Dixit at thoughtbot](https://www.youtube.com/watch?v=FtieBc3KptU) that is worth a watch.

I use [Evil layer](https://github.com/emacs-evil/evil) to bring Vim's modal editing capabilities to Emacs. I have to admit that Vim's editing ways are better that the default Keybindings for text manipulation used in Emacs. These are outright mental.

I write mostly in Emacs [Org Mode](https://orgmode.org/), which is an incredible layout/markup editing mode with myriad packages around to export and publish in most imaginable formats. Org Mode has redicoulous amount of functions, uses and capabilities, but this will be for another separate post.


## IDE {#ide}

{{< figure src="/images/emacs_lsp.gif" width="800px" >}}

I am not a developer by trade, but as a DevOps Engineer I do leave in code and scripts for a non insignificant time of my day. Which is why it makes me happy to no end that I can do all of that in my favorite application.

As mentioned above, Emacs is extensible, and there are more than enough packages to bring IDE chops to Emacs.

File Tree?
: Built-in with [Dired](https://wikemacs.org/wiki/Dired) or with packages like [Treemacs](https://github.com/Alexander-Miller/treemacs) or [Neotree](//github.com/jaypei/emacs-neotree).

Code Completion, Documentation, ?
: Try out [Company](https://github.com/company-mode/company-mode), LSP integration [lsp-mode](https://github.com/emacs-lsp/lsp-mode) or [eglot](https://github.com/joaotavora/eglot).

Syntax Checking?
: There is [Flycheck](https://www.flycheck.org/en/latest/) for that.

Debbuging?
: What about [dap-mode](https://github.com/emacs-lsp/dap-mode)!

{{< figure src="/images/emacs_lsp_error.gif" width="800px" >}}

There are to many possibilities to be mentioned here, but already with a bunch of built-in packages, modes and functions one can use Emacs wonderfully as an IDE for quite a number of languages.


## Git Client {#git-client}

The [Magit](https://github.com/magit/magit) package is a brilliant UI for git. All inside Emacs with keyboard driven menu, [transient commands](https://github.com/magit/transient). With Magit the complicated and numerous CLI git commands are made much easier and the options much more discoverable. The gif bellow is just a taste for the Magic. Get it? Magit/Magic.

{{< figure src="/images/emacs_magit.gif" width="800px" >}}


## Email Client {#email-client}

There are various methods/packages to transform Emacs into your (offline) email client. [Gnus](https://wikemacs.org/wiki/Gnus), [Notmuch](https://notmuchmail.org/notmuch-emacs/), [Mu4e](https://www.djcbsoftware.nl/code/mu/mu4e.html) and others. With more or less different philosophies of handling mails.

My choice is Mu4e, which is based on the CLI maildir format indexer `mu`. With Mu4e I have got a complete client which is a nice GUI-ish for my mails, that are stored in plain text on my machine.

For syncing with the IMAP Server I use `isync`, which is a third party tool to download my emails locally. And sending is handled by `msmtp`, CLI tool as well.

This includes all amenities, that I would need in an email client.

-   Plain text & hmtl email
-   Easy access and preview for attachments
-   Tag and folder handling
-   Sorting rules for email
-   Signature (and more) snippets
-   Inline preview of images
-   Threaded email list view
-   And and and

{{< figure src="/images/emacs_mu4e.png" width="800px" >}}


## RSS Reader {#rss-reader}

Nowadays it seems RSS feeds are a thing of the past, but for it is me a daily companion of self curated news. And [elfeed](https://github.com/skeeto/elfeed), the Emacs package I use makes it really easy for me to consume and handle.

Again, all in plain text inside Emacs with all functions one can need in a reader like filtering, categories, split views, html views, image previews, inline image views and more.

{{< figure src="/images/emacs_elfeed.png" width="800px" >}}


## File Manager {#file-manager}

The build in file manager [Dired](https://wikemacs.org/wiki/Dired) (short for directory editor, I believe) is quite amazing. Essentially a listing of the files in a directory similar to `ls -al`.

Navigation happens primarily through keybindings of course, but when working in the GUI version also with the mouse. You get the usual feature like in any Unix based CLI. Permissions listing etc.

{{< figure src="/images/emacs_dired.png" width="800px" >}}

The beautiful usefulness comes, when you take into account, that the listing is a buffer as well. Meaning it is editable, just like any other text file. Yes, the changes get applied to the listed files and/or directories.

I can recursively search directories with `find`, `ripgrep`, `grep`, `awk` or any other utilities and get the resulting files listed in a Dired buffer, edit them in the same buffer. Just imagine the possibilities.

I can even further narrow a listing with regex or just plain search and edit the results in the buffer.

Such "banalities" like copy/paste, move, permission modifications do not need to be mentioned of course. But here we are.

Well even images can be navigated in thumbnail view with larger previews. All in primarily text based application.

{{< figure src="/images/emacs_image_dired.png" width="800px" >}}


## PDF Tools {#pdf-tools}

Emacs has a PDF viewer, `DocView` built-in. But as all type of use cases the community has been at work and extended PDF functionality quite a bit.

I use [PDF tools](https://github.com/politza/pdf-tools) to get more functions in the viewer inside Emacs. This gives me the usual suspects like annotations, highlights, meta data display, search text, search and navigate links and more.

{{< figure src="/images/emacs_pdf_tools.png" width="800px" >}}

This being in Emacs though, I can also save links to PDF locations on hard drive and page numbers and insert them in other documents of mine. This makes organising my documents immensely easy.


## Docker Client {#docker-client}

For testing purposes, for the rare occasions a CLI program is not available on macOS or for a bunch of other reason, I like to have Docker and some images with different GNU/Linux distribution handy.

For some reasons I seem not to be able to remember the Docker CLI commands though. And Again Emacs and [docker.el](https://github.com/Silex/docker.el) to the rescue.

Just imagine a keyboard menu driven interface to list, inspect, run, delete, attach to (and more) Docker images, containers, volumes, network etc.

What about Kubernetes you ask. To steal a quote from come Apple's ads: "There is a package for that". I am mentioning docker.el here just to demonstrate how versatile Emacs is.

{{< figure src="/images/emacs_docker.png" width="800px" >}}


## GTD {#gtd}

The concept of tool enhanced getting things done is huge part of my work and private live. Being quite chaotic I need all the help that I can get.

`Org Mode` is fantastic in this case. I can capture my scheduled tasks, journal entries and other ToDos from everywhere into a text file. This comes with all the functionalities I need still. Reminders, desktop notification, scheduled emails and much more.

{{< figure src="/images/emacs_org.png" width="800px" >}}

Not only can I capture text that I type in, but I can link to most of the apps and locations on my Mac. With just a keybinding.

Imagine managing your calendar in plain text for posterity, but still having modern feature.

Emacs being scriptable, programmable and Emacs-lisp being a fully featured Language, I can access any imaginable API offline and online.

As mentioned before Org Mode merits its own post, hence me leaving it at that. For now i will leave you with a quote from the [official homepage](https://orgmode.org/):

> Org mode is for keeping notes, maintaining to-do lists, planning projects, authoring documents, computational notebooks, literate programming and more — in a fast and effective plain text system.

If you can't wait and want an appetizer, I suggest you watch a wonderful [introduction by daviwil](https://www.youtube.com/watch?v=PNE-mgkZ6HM). That should give you a taste of the possibilities.


## Blogging {#blogging}

Naturally I am typing these lines in Emacs. I use [hugo](https://gohugo.io/) for my static site based blog. There is a wonderful package, [ox-hugo](https://ox-hugo.scripter.co/), that let's me write my posts in Org Mode. The needed markdown files are created/updated on save.

{{< figure src="/images/emacs_hugo.png" width="800px" >}}

The resulting markdown files are correctly formatted, Including inline images, code blocks, quotes, tables and so on.

If needed I can include custom css in my org files and these get correctly exported to the markdown files and thus to the html files.


## Fun {#fun}

You want to take a break from work, just launch one of the casual games (Snake, Tetris, Pong, etc.) or talk to the doctor (more or less – probably less, but fun – an a.i.). Those are a few of the built-in package.

{{< figure src="/images/emacs_fun.png" width="800px" >}}

But Emacs being Emacs there are a bunch of install-able packages by the community for some serious procrastination. To name some:

-   [Pacmacs](https://github.com/codingteam/pacmacs.el)
-   [Sudoku](https://github.com/zevlg/sudoku.el)
-   [Slime Volleyball](https://github.com/fitzsim/slime-volleyball)
-   [Game of Life](https://github.com/Lindydancer/gameoflife)


## Conclusion {#conclusion}

Do not get discouraged from starting to use Emacs because of the learning curve. It is definitely worth the effort. The application was first released 45 years ago but is still in used and properly maintained. The community is very responsive and growing. You ll find more than enough resources on the internet to learn the use, either in text form or as video. Some of the contents that helped me will be linked at the bottom of this post.

{{< figure src="/images/emacs_learning_curve.png" width="800px" >}}


## Resources {#resources}

It didn't escape my notice that this post can only show you so much. For that I am listing some resources, that helped me on my journey.

These are in no particular order and sense:

[GNU Emacs Manual](https://www.gnu.org/software/emacs/manual/html%5Fnode/emacs/index.html)
: Let's start with the official documentation.

Emacs Tutorial
: Built-In getting started tutorial.

[Org Mode turorial videos by Rainer König](https://www.youtube.com/watch?v=sQS06Qjnkcc&list=PLVtKhBrRV%5FZkPnBtt%5FTD1Cs9PJlU0IIdE)
: Fantastic Videos with an accent.

[Emacs from scratch video series](https://www.youtube.com/watch?v=74zOY-vgkyw&list=PLEoMzSkcN8oPH1au7H6B7bBJ4ZO7BXjSZ)
: Getting to learn Emacs from the start.

[M-x emacs-reddit](https://www.reddit.com/r/emacs/)
: Reddit has to be mentioned of course.

[One-page guide to learning Emacs](https://sachachua.com/blog/begin-emacs)
: Fantastic visual tutorial by Sacha Chua.

[Emacs on StackExchange](https://emacs.stackexchange.com)
: If you got questions, they might have been answered.

[Awesome Emacs](https://github.com/emacs-tw/awesome-emacs)
: List of useful Emacs packages, utilities and libraries.

[Emacs Redux](https://emacsredux.com)
: Blog with somewhat Emacs topics.

[pragmatic emacs](http://pragmaticemacs.com)
: Another blog about and around Emacs.

[Practical Emacs Tutorial](http://ergoemacs.org/emacs/emacs.html)
: Designed for writers, scientists, engineers.

[Your life in plain text](https://orgmode.org/)
: Official Homepage for Org Mode.

[Org Mode 4 Beginners](https://orgmode.org/worg/org-tutorials/org4beginners.html)
: Org Mode beginning at the basics.
