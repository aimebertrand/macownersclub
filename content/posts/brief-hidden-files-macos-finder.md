+++
title = "Brief #1 - Hidden files in macOS Finder"
author = ["Aimé Bertrand"]
date = 2022-08-07T00:00:00+02:00
lastmod = 2022-08-07T21:49:57+02:00
tags = ["macos", "finder", "files"]
categories = ["macos"]
draft = false
+++

## Intro {#intro}

This is the first of – hopefully – many posts with brief/short How-Tos and more.

Today we are talking about a few methods on how to hide ans reveal files in macOS `Finder.app`.

{{< figure src="/images/finder-app.png" width="100px" >}}


## Show hidden files in the finder {#show-hidden-files-in-the-finder}


### Keyboard shortcut {#keyboard-shortcut}

The easiest way of showing hidden files/folders in is with a keyboard shortcut. With the Finder focused hit:

`cmd + shift + .`

This toggles the visibility of hidden files.


### Command line {#command-line}

The second method – rather more useful in automation and scripting – is on the command line.

**Show hidden files:**

```sh
defaults write com.apple.finder AppleShowAllFiles 1
killall Finder # restart Finder.app
```

{{< figure src="/images/finder-not-hidden.png" width="400px" >}}

**Hide hidden files:**

```sh
defaults write com.apple.finder AppleShowAllFiles 0
killall Finder # restart Finder.app
```

{{< figure src="/images/finder-hidden.png" width="400px" >}}


## Methods to hide a file in the Finder {#methods-to-hide-a-file-in-the-finder}


### Unix method {#unix-method}

On most Unix like Systems hiding a directory is as easy as prepending a `.` to the file or folder name. This is of course the case on macOS as well.


### File flag method {#file-flag-method}

macOS comes with the command line program `chflags`, which among others can change the visibility in the Finder. See the following commands.

**Hide files/folders:**

```sh
chflags hidden ~/Library # hide
```

**To reverse this:**

```sh
chflags nohidden ~/Library # unhide
```

Contrary to the first method this one does not hider the files on the command line. Meaning a simple `ls` will list the files/folders on the terminal.


## Conclusion {#conclusion}

macOS as a family member of the Unix like OSs has enough methods of hiding and revealing files on the file system to fit any need. The methods above are by no mean all of them. Get at it!