+++
title = "Emacs and macOS like keybindings"
author = ["Aimé Bertrand"]
date = 2021-12-05T00:00:00+01:00
lastmod = 2021-12-05T22:23:51+01:00
tags = ["emacs", "keybindings", "macos"]
categories = ["emacs"]
draft = false
+++

## Why would I do that? {#why-would-i-do-that}

I use macOS – as evident by my domain. I started using it for university some 20 years ago and never looked back. This is also the case in my work life.

{{< figure src="/images/mac_kb.png" width="400px" >}}

As consequence of this I am quite committed to the macOS keybindings. So starting using Emacs about 2 years ago it was quite clear to me that...

-   One, I can't handle the default keybindings and especially key chords
-   And two, I don't want to retrain my muscle memory

Well since Emacs is incredibly bendy, this was not a hard thing to do.


## How then? {#how-then}

First things first. This is not about `evil-mode`, which I admittedly use instead of the vanilla keybindings for editing text. In this case we are just talking about non-text-editing bindings. Those to call commands, interactive functions, programs, ui navigation etc. Also I got nothing of use to add to the myriad of posts, articles and more about the evil package.

Now...

The basis for most or maybe all shortcuts on macOS is the `command` key (aka `cmd` key; aka `apple` key; aka `⌘` key).

Say you want to save a file, this would be `⌘-s` in most if not all applications. Similarly copy is `⌘-c` and paste is `⌘-v`. You get the idea. Together with `control` (⌃) and `option` (⌥) one has access to a whole host of possible combinations.

To be able to put all of this into Emacs-Speak, I adjust one variables:

```emacs-lisp
(customize-set-variable 'mac-command-modifier 'meta) ; make cmd key do Meta
```

A little snag on the German keyboard on macOS is the fact, that `option` is a modifier to get access to special characters. It is but a scratch though. All I have to do is incorporate those characters into my keybinding definitions. I.e. `option-e` for `€`.

All of the above translates into the following possible prefixes:

macOS Keys
: ctrl, option, cmd, ctrl-option, ctrl-cmd, option-cmd, ctrl-option-cmd

With that I rarely use key chords, I rather hold down 3 keys and press another for a keybinding. Like I said, muscle memory.

Few examples:

```emacs-lisp
;; option-c = ç:
(global-set-key (kbd "C-M-ç") 'timu/make-capture-frame) ; (ctrl-option-cmd-c)
;; option-w = ∑:
(global-set-key (kbd "C-∑") 'writeroom-mode) ; (ctrl-option-w)

(define-key vertico-map (kbd "M-v") 'yank) ; (cmd-v)
(global-set-key (kbd "M-s") 'save-buffer) ; (cmd-s)
(global-set-key (kbd "M-ç") 'org-capture) ; (option-cmd-c)
```


## Conclusion {#conclusion}

Is all the fuss changing quite a lot of default bindings worth the hassle?

Since most of my keybindings work in the same manner as the rest of the OS for me, the context switching (from and to Emacs) does not feel much like one.

Plus... Let us be honest. Tweaking Emacs is fun too.