+++
title = "Learning Python #1"
author = ["Aimé Bertrand"]
date = 2021-02-21T00:00:00+01:00
lastmod = 2021-04-23T14:59:47+02:00
tags = ["coding", "learning", "development"]
categories = ["python"]
draft = false
+++

## Little history/background {#little-history-background}

Almost 2 years ago I decided to learn to code and my language of choice would be Python. I worked as an Administrator and thought to myself that a multi purpose language would do the trick.

The next thing would be to choose a proper editor/IDE. So the search started and lasted till now.

In that time I learned to use vim properly and I view myself as quite proficient in it now. From there I discovered Emacs and learned to love it. I use it in combination with evil-mode, because the Emacs native Keybindings annoy me to no end.

I use Emacs for almost everything now:

-   Scripting
-   Writing
-   Emails (mu4e)
-   Reading the News (elfeed)
-   Editor/IDE
-   etc ...

Don't get me started with Org-mode and all I can do with it (ToDos, elaborate note taking system, fast capturing of all manner of information and much much more).

Now that what one calls proper procrastination. No regrets here though since Emacs has now turned into my favorite peace of software – by far.


## Starting to code in Python {#starting-to-code-in-python}

I recently and finally started learning to code. It is mostly through videos, reading documentations and looking at other peoples examples.

I now finished the first working version of a script, that I need. Well need i strong word. There are a bunch of solutions and Apps to what I wanna achieve here, but I want to learn after all. So...

What the script does? It looks for files in a given folder on my machine with the name in the form "YYYY-MM-DD File Name.extension" and move them in a destination folder in the form "destination/YYYY/MM". This is my way of organizing/archiving my files.

This is the code:

```python
#!/usr/local/bin/python3

# import modules
import os

# 1. change to wd
homedir = os.environ['HOME']
wdir = os.path.join(homedir, 'projects/python/file-sorting/inbox')
destdir = os.path.join(homedir, 'projects/python/file-sorting/sorted')

# 2 list files to check (fmove)
os.chdir(wdir)
wdcontent = os.listdir(wdir)

# 2.1. remove directories & unwanted files from the list
flist = list()
for f in wdcontent:
    if f.startswith('.'):
        pass
    elif f == '.DS_Store':
        pass
    else:
        fpath = os.path.join(wdir, f)
        if os.path.isfile(fpath):
            flist.append(f)

# 2.2 check if files have date in name
yearlist = list(range(1900, 2100))

years = list()
for y in yearlist:
    years.append(str(y))

months = ['01', '02', '03', '04', '05',
          '06', '07', '08', '09', '10', '11', '12']

fmove = list()
for f in flist:
    sf = f.split('-')
    if sf[0] in years and sf[1] in months:
        fmove.append(f)

# 4. splitt year and month from name and store in var
for f in fmove:
    sf = f.split('-')
    fyear = sf[0]
    fmonth = sf[1]
    fdestdir = os.path.join(destdir, fyear, fmonth)
    fsrc = os.path.join(wdir, f)
    fdest = os.path.join(fdestdir, f)
    if os.path.isdir(fdestdir):
        os.rename(fsrc, fdest)
    else:
        os.makedirs(fdestdir)
        os.rename(fsrc, fdest)
```

I bet this can be improved of course. But that is the point. I am just starting and the idea is to improve on it while learning.

I will be documenting my progress here for me and for anyone interested. Who knows, this might be helpful.


## Next steps for this script {#next-steps-for-this-script}

-   Add conditions to avoid duplicates
-   Maybe use classes and custom functions
-   What ever else I will come up with
