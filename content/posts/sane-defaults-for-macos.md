+++
title = "Sane defaults for macOS"
author = ["Aimé Bertrand"]
date = 2021-08-04T00:00:00+02:00
lastmod = 2021-08-04T22:40:11+02:00
tags = ["macos", "preferences"]
categories = ["macos"]
draft = false
+++

## Motivation {#motivation}

I love macOS. My job revolves around it, I write about it on this site and I have been using it for about 20 years with no major complaint. Hell, this domain starts with the word "mac".

{{< figure src="/images/apple_old_logo.png" width="100px" >}}

However some of the behaviours in the UI that I have been used to over the years are not set by defaults. It would drive me mental not having these.

As most of my post, this is for my own documentation, but in the spirit of sharing I hope that this might help someone out there.

First things first...


## Dependencies {#dependencies}

This section – strictly speaking – does not talk about macOS defaults. But I absolutely need these tools to be able to set up my Mac fast, comfortably and properly.

-   **_Homebrew:_** Command line package manager for macOS. I install most of my programs with it. How to install it is described on the [homepage](https://brew.sh).

{{< figure src="/images/homebrew.png" width="80px" >}}

-   **_dockutil:_** Command line utility for managing Mac OS X dock items. Installed via homebrew.
-   **_mysides:_** Command line tool for Finder sidebar modification. Installed via homebrew as well.


## Changing settings {#changing-settings}

Personally I have scripted these settings to be done automatically. Some snippets are stolen of course 😎.  For this post however I ll mention the settings over the GUI as well.


### Finder & Desktop {#finder-and-desktop}

This are the settings you can change over the Finder preferences (`⌘ + ,`) or the view options (`⌘ + j`).

{{< figure src="/images/finder_prefs.png" width="400px" >}}

-   _Always show file suffixes:_ macOS does not need this to know the file type, but I do. The icon previews are by far not enough.
-   _Always search the current folder:_ For global search on disk I user either `Spotlight` or `Alfred.app`
-   _Show the path bar and the status bar:_ This is quite spiffy since it provides the location in the Finder and the available space on disk at a glance. Plus I prefer the aesthetics.

<img style="display:inline;float:left;margin:10px 40px;" src="/images/finder_path_status.png" width="600"/>

-   _Visible devices in the sidebar:_ I like to see both hard disks, removable devices and servers in my sidebar for quicker access.
-   _Items to display on the desktop:_ To maximise the quick access I want to all manner of volumes on my desktop as well.
-   _Open new Finder windows showing the home directory:_ This might just be a matter of taste.
-   _Open new Finder windows in list view:_ The default, which is the icon view drives me up the walls.

In addition to the GUI, these settings can be changed with a simple bash script. This is the corresponding snippet of the script.

```sh
# Always show suffixes
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# Always search current folder
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

# Show path bar and status bar
defaults write com.apple.finder ShowStatusBar -bool true
defaults write com.apple.finder ShowPathbar -bool true

# Devices for the sidebar
defaults write com.apple.sidebarlists systemitems -dict-add ShowServers -int 1
defaults write com.apple.sidebarlists systemitems -dict-add ShowRemovable -int 1
defaults write com.apple.sidebarlists systemitems -dict-add ShowHardDisks -int 1
defaults write com.apple.sidebarlists systemitems -dict-add ShowEjectables -int 1

# Items to display on the desktop
defaults write com.apple.finder ShowHardDrivesOnDesktop -int 1
defaults write com.apple.finder ShowMountedServersOnDesktop -int 1
defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -int 1
defaults write com.apple.finder ShowRemovableMediaOnDesktop -int 1

# Open home in new window
defaults write com.apple.finder NewWindowTarget -string "PfLo"
defaults write com.apple.finder NewWindowTargetPath -string "'file://$HOME/"

# List view in by default
# Possible: `icnv`, `clmv`, `Flwv`
defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"

# Show the ~/Library folder
chflags nohidden ~/Library
```


### Dock {#dock}

I general, there is not much to the Dock.app. But a few fixes/adjustments go a long way as a matter of quality of life. Most of these settings are accessible through the `System Preferences.app` or a simple drag and drop.

-   _Remove everything from the dock:_ I keep the following items in the beginning and add others as I go:
    -   AppCleaner.app
    -   App Store.app
    -   Messages.app
    -   System Preferences.app
    -   iTerm.app
    -   Safari.app

<img style="display:inline;float:left;margin:10px 40px;" src="/images/dock_prefs.png" width="600"/>

-   _Automatically hide and show the Dock:_ Since I work quite a lot on the MacBook I take all the real estate of the screen that I can get. However for this change not to drive me nuts, I need the popup of the hidden Dock to be significantly faster than the default settings. This is unfortunately this is only possible via the command line.

The complete snippet for the Dock settings:

```sh
# Dock: Make it popup faster
defaults write com.apple.dock autohide-delay -float 0
defaults write com.apple.dock autohide-time-modifier -float 0

# Automatically hide and show the Dock
defaults write com.apple.dock autohide -int 1

# Remove everything
dockutil --remove all > /dev/null 2>&1

# Minimal setup
dockutil --add /Applications/AppCleaner.app > /dev/null 2>&1
dockutil --add /System/Applications/App\ Store.app > /dev/null 2>&1
dockutil --add /System/Applications/Messages.app > /dev/null 2>&1
dockutil --add /System/Applications/System\ Preferences.app > /dev/null 2>&1
dockutil --add /Applications/iTerm.app > /dev/null 2>&1
dockutil --add /Applications/Safari.app > /dev/null 2>&1
```


### General UI behaviour {#general-ui-behaviour}

-   _Scroll bars visibility:_ For a few macOS versions now the scroll bar has been reasonably discrete and small enough, which is why I like it to be always around.

<img style="display:inline;float:left;margin:10px 40px;" src="/images/scroll_bars.png" width="600"/>

-   _Right click for magic mouse:_ God knows why this is not the defaults behaviour.
-   _Tap and Two-Finger-Tap for the trackpad:_ I can't really say when I use a click or tap on the trackpad, but I need the right click with the two-finger-tap for sure.

<img style="display:inline;float:left;margin:10px 40px;" src="/images/mouse_trackpad.png" width="600"/>

-   _Much faster keyboard key repeat:_ This is of at most importance for navigating through text. For the speed I need the settings in the GUI isn't not enough. I change this in a script.
-   _Turn off auto correct & auto capitalize:_ This would be otherwise annoying to no end.

<img style="display:inline;float:left;margin:10px 40px;" src="/images/keyboard_prefs.png" width="600"/>

-   _Different spaces for different displays:_ Probably a matter of taste again.
-   _Do not automatically rearrange Spaces based on most recent use:_ I like to know where all my windows are and don't want macOS to reorder this automatically.
-   _Hot corners:_ I can't work without these. I Use them to access the desktop, call up `Exposé` and start the screensaver which also locks the Mac straight away.

<img style="display:inline;float:left;margin:10px 40px;" src="/images/hot_corners.png" width="600"/>

Here is how I implement the settings in a script:

```sh
# Scroll bars
# Possible: "WhenScrolling", "Automatic" and "Always"
defaults write NSGlobalDomain AppleShowScrollBars -string "Always"

# Right click for magic mouse
defaults write com.apple.AppleMultitouchMouse MouseButtonMode -string TwoButton

# System Preferences: Trackpad
# Trackpad: Tap
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults write com.apple.AppleMultitouchTrackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
# Trackpad: Two-Finger-Tap
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
defaults write com.apple.AppleMultitouchTrackpad TrackpadRightClick -bool true
defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true
defaults write com.apple.AppleMultitouchTrackpad TrackpadRightClick -bool true

# Keyboard key repeat
defaults write -g InitialKeyRepeat -int 10
defaults write -g KeyRepeat -int 1

# Auto correct off & Auto capitalize off
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false
defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false

# Different spaces for different displays
defaults write com.apple.spaces spans-displays -int 0
# Don’t automatically rearrange Spaces based on most recent use
defaults write com.apple.dock mru-spaces -int 0

# Hot corners
# Possible values: 0 no-op; 2 Mission Control; 3 Show application windows;
# 4 Desktop; 5 Start screen saver; 6 Disable screen saver; 7 Dashboard;
# 10 Put display to sleep; 11 Launchpad; 12 Notification Center
defaults write com.apple.dock wvous-tl-corner -int 3
defaults write com.apple.dock wvous-tl-modifier -int 0

defaults write com.apple.dock wvous-tr-corner -int 4
defaults write com.apple.dock wvous-tr-modifier -int 0

defaults write com.apple.dock wvous-bl-corner -int 2
defaults write com.apple.dock wvous-bl-modifier -int 0

defaults write com.apple.dock wvous-br-corner -int 5
defaults write com.apple.dock wvous-br-modifier -int 0
```
