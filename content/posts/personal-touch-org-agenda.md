+++
title = "A Personal Touch in my Org Agenda"
author = ["Aimé Bertrand"]
date = 2024-05-26T00:00:00+02:00
lastmod = 2024-05-26T22:43:11+02:00
tags = ["emacs", "org", "agenda", "orgmode"]
categories = ["orgmode"]
draft = false
+++

Hello there!

Today, I want to delve into my custom Org Agenda settings, shedding light on how my workflow compares to the default configurations.


## Why Customize Org Agenda? {#why-customize-org-agenda}

The default Org Agenda in Emacs is powerful, but with a few tweaks, it can be tailored to fit your personal workflow like a glove. Here's a peek into my settings and why they make a difference.


## Cleaner and Clearer Layout {#cleaner-and-clearer-layout}

First up, I've removed tags from the agenda view:

```emacs-lisp
(customize-set-variable 'org-agenda-remove-tags t)
```

This keeps my agenda clutter-free and focused on the tasks at hand.

I've also customized the prefix and keyword formats:

```emacs-lisp
(customize-set-variable 'org-agenda-prefix-format " %-12c %?-14t% s")
(customize-set-variable 'org-agenda-todo-keyword-format "%-10s")
```

This ensures everything is neatly aligned, making it easier to scan through my tasks.


## Distinct Scheduling and Deadlines {#distinct-scheduling-and-deadlines}

To clearly differentiate between scheduled tasks and deadlines, I've set up specific leaders:

```emacs-lisp
(customize-set-variable 'org-agenda-scheduled-leaders
                        '("[S] : " "[S] x%3d d.: "))
(customize-set-variable 'org-agenda-deadline-leaders
                        '("[D] : " "[D] +%3d d.: " "[D] -%3d d.: "))
```

This visual cue helps me prioritize my day at a glance.


## Enhanced Time Management {#enhanced-time-management}

I've refined the time grid and added a current time string:

```emacs-lisp
(customize-set-variable 'org-agenda-time-grid
                        '((today require-timed remove-match)
                          (000 1200 2400)
                          ":  " "┈┈┈┈┈┈┈┈┈┈┈┈┈"))
(customize-set-variable 'org-agenda-current-time-string "ᐊ┈┈┈┈┈┈┈┈ now")
```

These adjustments make it easy to keep track of my schedule and current time visually.


## Custom Commands for Specific Views {#custom-commands-for-specific-views}

I've added custom commands to cater to different needs:

-   **Weekly Overview**:

<!--listend-->

```emacs-lisp
(add-to-list
 'org-agenda-custom-commands
 '("w" "THIS WEEK"
   ((agenda ""
            ((org-agenda-overriding-header
              (concat "THIS WEEK (W" (format-time-string "%V") ")")))))))
```

-   **Daily Agenda**:

<!--listend-->

```emacs-lisp
(add-to-list
 'org-agenda-custom-commands
 '("d" "DAY'S AGENDA"
   ((agenda ""
            ((org-agenda-overriding-header
              (concat "TODAY (W" (format-time-string "%V") ")"))
             (org-agenda-span 'day)
             (org-agenda-sorting-strategy
              '((agenda time-up priority-down category-keep)))
             (org-agenda-show-log t)
             (org-agenda-log-mode-items '(clock)))))))
```

-   **Custom Overview**:

<!--listend-->

```emacs-lisp
(add-to-list
 'org-agenda-custom-commands
 '("c" "CUSTOM OVERVIEW"
   ((tags-todo "+PRIORITY=\"A\""
               ((org-agenda-overriding-header "PRIO A")))
    (agenda ""
            ((org-agenda-overriding-header
              (concat "TODAY (W" (format-time-string "%V") ")"))
             (org-agenda-span 'day)
             (org-agenda-sorting-strategy
              '((agenda time-up priority-down category-keep)))
             (org-agenda-show-log t)
             (org-agenda-log-mode-items '(clock))))
    (agenda ""
            ((org-agenda-overriding-header
              (concat "FOLLOWING DAYS (W" (format-time-string "%V") ")"))
             (org-agenda-skip-function
              '(org-agenda-skip-entry-if 'unscheduled))
             (org-agenda-span 6)
             (org-agenda-start-day "+1d")
             (org-agenda-start-on-weekday nil)))
    (tags-todo "+private"
               ((org-agenda-overriding-header "PRIVATE TASKS")
                (org-agenda-skip-function
                 '(org-agenda-skip-entry-if 'unscheduled))))
    (tags-todo "+work"
               ((org-agenda-overriding-header "WORK TASKS")
                (org-agenda-skip-function
                 '(org-agenda-skip-entry-if 'unscheduled))))
    (tags "CLOSED>=\"<-7d>\"|DONE>=\"<-7d>\"|CANCELLED>=\"<-7d>\""
          ((org-agenda-overriding-header "Completed in the Last 7 Days\n"))))))
```

These commands offer tailored views, whether I need a weekly snapshot, today's agenda, or a categorized task overview.


## Optimized Display {#optimized-display}

Finally, I've configured the display settings to ensure the agenda doesn't obstruct my main workspace:

```emacs-lisp
emacsCopy code(add-to-list 'display-buffer-alist '("\\*Agenda Commands\\*" (display-buffer-in-side-window) (side . right) (window-width . 0.4)))
(add-to-list 'display-buffer-alist '("\\*Org Agenda\\*" (display-buffer-in-side-window) (side . right) (window-width . 0.5)))
```

This keeps my workflow smooth and my screen space efficiently used.


## Conclusion {#conclusion}

By customizing my Org Agenda settings, I've crafted a tool that fits my workflow, enhancing both productivity and task management.

Happy tweaking!


## Disclaimer {#disclaimer}

<style>.disclaimer {color: #d08770;}</style>

<div class="disclaimer">

This abomination of a post was written by ChatGPT 4o. The prompt:

</div>

```text
The following is my org agenda config in my Emacs... I want you to write me a post in the style of my other posts about my org agenda settings and if possible also the advantages compared to the default settings in emacs:

...
Org Agenda Config code
...
```

<style>.disclaimer {color: #d08770;}</style>

<div class="disclaimer">

The first draft was an non-starter which required some addition prompting:

</div>

```text
This does not feel like my style. Its just like a bulleted list. Feel free to browse my blog and see how my style ist.
```

<style>.disclaimer {color: #d08770;}</style>

<div class="disclaimer">

(see my  [Org Mode Config](https://gitlab.com/aimebertrand/dotemacs/-/blob/main/libraries/timu-org.el))

I think it's gonna be a while until I either am happy with the results or am a passable "prompt engineer" to get a good post with just LLM Chatbots.

But since I did put in some minimal effort... here are 😜

</div>
