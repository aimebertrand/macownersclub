+++
title = "Sudo with Touch ID on the macOS Terminal"
author = ["Aimé Bertrand"]
date = 2021-12-14T00:00:00+01:00
lastmod = 2024-02-13T22:51:27+01:00
tags = ["macos", "sudo", "terminal", "touch-id"]
categories = ["macos"]
draft = false
+++

<style>.orange {color: #d08770;}</style>

<div class="orange">

**UPDATE - 13. Feb. 2024:**

</div>

From [What’s new for enterprise in macOS Sonoma](https://support.apple.com/en-gb/HT213893):

> Touch ID can be allowed for `sudo` with a configuration that persists across software updates using `/etc/pam.d/sudo_local`. See `/etc/pam.d/sudo_local.template` for details.

This means the following steps will give you the result:

1.  `sudo cp /etc/pam.d/sudo_local.template /etc/pam.d/sudo_local`

2.  Remove the comment `#` from the 3. line. This should be the result:

<!--listend-->

```bash
# sudo_local: local config file which survives system update and is included for sudo
# uncomment following line to enable Touch ID for sudo
auth       sufficient     pam_tid.so
```


## Motivation {#motivation}

I am lazy. Need I say more?

Listen, this is not a major thing, but I use sudo extensively. Therefore typing my password over and over again gets tedious at some point.

Lucky for me there is a solution...

Touch ID as an authentication method for sudo was at least envisioned by apple as it seems. However it is deactivated by default.

{{< figure src="/images/sudo-touch-id-dark.png" width="300px" >}}


## How {#how}

The file `/etc/pam.d/sudo` contains the authentication methods and rule sets for `sudo` on the command line.

All that remains is adding `auth sufficient pam_tid.so` at the beginning of the file. You will require sudo permissions to be able to write to the file.

```sh
# sudo: auth account password session
auth       sufficient     pam_tid.so # new line
auth       required       pam_opendirectory.so
account    required       pam_permit.so
password   required       pam_deny.so
session    required       pam_permit.so
```

**Bonus:** This works for all of the sudo prompts in my Emacs as well.

**Be advised:**

> You are messing with permission escalation here. Do be aware of the risks involved here. Read up if necessary.


## iTerm2 {#iterm2}

This works with all the Terminal emulators, that I have used so far (Terminal, Kitty, Alacritty,...) out of the box.

However iTerm2 seems to be a diva in that regard. Meaning, that it needs more settings to go along. This is illustrated in this [GitLab issue](https://gitlab.com/gnachman/iterm2/-/issues/7608).

-   Go to `Preferences > Advanced`
-   Search for `Allow sessions to survive after logging out and back in`
-   Select "No" in the drop-down.

{{< figure src="/images/sudo-touch-id-iterm.png" width="600px" >}}
