---
title: About
date: 2020-11-23 17:00:00
---

# About

I am DevOps Engineer based in Hamburg and trying to keep up. Might be a curse or a blessing but it turns out that IT is my hobby as well.

<img style="margin-top: -30px; margin-bottom: -30px;" src="../images/my-memoji.png" width="200" height="200" />

Making this site to:

1. Cope with the aforementioned fact.
2. Share my love for macOS and (open-source) software.
3. Claim my corner of the Internet.
4. Few other reasons...
